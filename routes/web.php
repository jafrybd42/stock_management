<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\InvestorController;
use App\Http\Controllers\PartnerController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductHeadController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductGroupController;
use App\Http\Controllers\ProductSizeController;
use App\Http\Controllers\ProductTypeController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\StocksController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\CartsController;
use App\Http\Controllers\CustomerReturnController;
use App\Http\Controllers\InvoiceTranController;
use App\Http\Controllers\SupplierReceiveController;
use App\Http\Controllers\SupplierReturnController;
use App\Http\Controllers\GeneralExpenseController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\RegularExpenseController;
use App\Http\Controllers\TotalExpenseController;
use App\Http\Controllers\BanksController;
use App\Http\Controllers\StockOpeningEndingRecordsController;
use App\Http\Controllers\AdminController;
use App\Models\ProductCategory;
use App\Models\ProductSize;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/change/password', [App\Http\Controllers\HomeController::class, 'changePasswordUI'])->name('changePasswordUI');
Route::post('/change-password', [App\Http\Controllers\HomeController::class, 'changePasswordConfirm'])->name('changePasswordConfirm');

Route::resource('admins',AdminController::class);
Route::post('/admins/updateAdmin/', [AdminController::class,'updateAdmin'])->name('admins.updateAdmin'); 

Route::resource('product-head',ProductHeadController::class);
Route::resource('product-category',ProductCategoryController::class);
Route::resource('product-group',ProductGroupController::class);
Route::resource('product-type',ProductTypeController::class);
Route::resource('product-size',ProductSizeController::class);
Route::resource('supplier',SupplierController::class);
Route::resource('customer',CustomerController::class);
Route::resource('product',ProductsController::class);
Route::resource('partner',PartnerController::class);
Route::post('/partner/updateMoney/', [PartnerController::class,'moneyUpdate'])->name('partner.moneyUpdate'); 
Route::get('/partnerBalance/{id}', [PartnerController::class,'balanceHistory'])->name('partner.balanceHistory');
Route::resource('expense',ExpenseController::class);
Route::resource('investor',InvestorController::class);
Route::post('/investor/updateMoney/', [InvestorController::class,'moneyUpdate'])->name('investor.moneyUpdate'); 
Route::get('/investorBalance/{id}', [InvestorController::class,'balanceHistory'])->name('investor.balanceHistory');
Route::resource('stock',StocksController::class);
Route::post('/stock/price-stock-change/{id}', [StocksController::class,'price_stock_change'])->name('stock.price_stock_change');
Route::get('/reOrderList', [StocksController::class,'reOrderList'])->name('stock.reOrderList');

Route::resource('sales',SalesController::class);
Route::post('/sales/addToCart/', [SalesController::class,'add_to_cart'])->name('sales.add_to_cart');
Route::resource('carts',CartsController::class);
Route::post('/carts/sales/checkout/', [CartsController::class,'sales_checkout'])->name('carts.sales_checkout');
Route::delete('/carts/sales/remove/', [CartsController::class,'remover_from_carts'])->name('carts.remover_from_carts');
Route::post('/carts/sales/updates/', [CartsController::class,'info_update'])->name('cart.info_update');

Route::post('/customer/info/', [CustomerController::class,'getCustomerInfoByPhoneNo'])->name('customer.getCustomerInfoByPhoneNo');
Route::post('/customer/payment/', [CustomerController::class,'payment'])->name('customer.payment');
Route::get('/customerBalance/{id}', [CustomerController::class,'balanceHistory'])->name('customer.balanceHistory');
Route::get('/customer-due-list', [CustomerController::class,'dueList'])->name('customer.dueList');

Route::post('/supplier/info/', [SupplierController::class,'getSupplierInfoByPhoneNo'])->name('supplier.getSupplierInfoByPhoneNo');
Route::post('/supplier/payment/', [SupplierController::class,'payment'])->name('supplier.payment');
Route::get('/supplierBalance/{id}', [SupplierController::class,'balanceHistory'])->name('supplier.balanceHistory');

Route::post('/product-head/editModal', [ProductHeadController::class,'modalEdit'])->name('product-head.modalEdit');
Route::post('/product/search/', [ProductsController::class,'search'])->name('product.search');
Route::get('/partners/pdf', [PartnerController::class, 'createPDF']);
Route::post('/product/details/', [ProductsController::class,'detailsByID'])->name('product.detailsByID');

Route::get('/customer-return', [CustomerReturnController::class,'index'])->name('customer_return.index');
Route::post('/customer-return/addToCart/', [CustomerReturnController::class,'add_to_cart'])->name('customer_return.add_to_cart');
Route::get('/customer-return-cart', [CustomerReturnController::class,'selectedCarts'])->name('customer_return.selectedCarts');
Route::delete('/customer-return/remove/', [CustomerReturnController::class,'remove_from_cart'])->name('customer_return.remove_from_cart');
Route::post('/carts/customer-return/updates/', [CustomerReturnController::class,'info_update'])->name('customer_return.info_update');
Route::post('/customer-return/checkout/', [CustomerReturnController::class,'sales_checkout'])->name('customer_return.sales_checkout');


Route::get('/supplier-receive', [SupplierReceiveController::class,'index'])->name('supplier_receive.index');
Route::post('/supplier-receive/addToCart/', [SupplierReceiveController::class,'add_to_cart'])->name('supplier_receive.add_to_cart');
Route::get('/supplier-receive-cart', [SupplierReceiveController::class,'selectedCarts'])->name('supplier_receive.selectedCarts');
Route::delete('/supplier-receive/remove/', [SupplierReceiveController::class,'remove_from_cart'])->name('supplier_receive.remove_from_cart');
Route::post('/carts/supplier-receive/updates/', [SupplierReceiveController::class,'info_update'])->name('supplier_receive.info_update');
Route::post('/supplier-receive/checkout/', [SupplierReceiveController::class,'buing_checkout'])->name('supplier_receive.buing_checkout');


Route::get('/supplier-return', [SupplierReturnController::class,'index'])->name('supplier_return.index');
Route::post('/supplier-return/addToCart/', [SupplierReturnController::class,'add_to_cart'])->name('supplier_return.add_to_cart');
Route::get('/supplier-return-cart', [SupplierReturnController::class,'selectedCarts'])->name('supplier_return.selectedCarts');
Route::delete('/supplier-return/remove/', [SupplierReturnController::class,'remove_from_cart'])->name('supplier_return.remove_from_cart');
Route::post('/carts/supplier-return/updates/', [SupplierReturnController::class,'info_update'])->name('supplier_return.info_update');
Route::post('/supplier-return/checkout/', [SupplierReturnController::class,'checkout'])->name('supplier_return.checkout');

Route::get('/invoice/advance-sales', [InvoiceTranController::class,'advanceSales'])->name('invoice.advance_sales_list');

Route::get('/invoice', [InvoiceTranController::class,'index'])->name('invoice.list');
Route::get('/invoice/{id}', [InvoiceTranController::class,'show'])->name('invoice.details');

Route::post('/invoice/complete', [InvoiceTranController::class,'complete_advance_sales'])->name('invoice.complete_advance_sales');
Route::post('/invoiceSearch', [InvoiceTranController::class,'invoiceSearch'])->name('invoice.invoiceSearch');
Route::get('/pdf/{id}', [InvoiceTranController::class,'generatePdf'])->name('invoice.generatePdf');



/// General Expense + Regular Expense + Total Expense
Route::resource('general-expense',GeneralExpenseController::class);
Route::resource('regular-expense',RegularExpenseController::class);
Route::post('/regular-expense/search/', [RegularExpenseController::class,'search'])->name('regular-expense.search');
Route::any('/total-expense', [TotalExpenseController::class,'index'])->name('total-expense.index');

// Account

Route::get('/journal', [JournalController::class,'journal'])->name('journal.list');
Route::get('/trial-balance', [JournalController::class,'trial_balance'])->name('journal.trial_balance');
Route::get('/income-statement', [JournalController::class,'income_statement'])->name('journal.income_statement');
Route::get('/owner-equity', [JournalController::class,'owner_equity'])->name('journal.owner_equity');
Route::get('/balance-sheet', [JournalController::class,'balance_sheet'])->name('journal.balance_sheet');
Route::any('/stock-opening-ending-reports', [StockOpeningEndingRecordsController::class,'index'])->name('stock.stock_opening_ending');



/// Bank

Route::resource('bank',BanksController::class);
Route::post('/user/name/', [BanksController::class,'getUserName'])->name('bank.getUserName');


// reports Supplier
Route::any('/reports/suppliers-date-wise-purchase', [SupplierController::class,'suppliers_date_wise_purchase'])->name('reports.date-wise-purchase');
Route::any('/reports/product-wise-sales-reports-month', [SupplierController::class,'product_wise_sales_reports_month'])->name('reports.product-wise-sales-reports-month');
Route::any('/reports/cash-flow', [JournalController::class,'cash_flow'])->name('reports.cash-flow');



// Route::get('/product-head', [App\Http\Controllers\ProductHeadController::class,'index'])->name('product_head.index');
// Route::post('/product-head', [App\Http\Controllers\ProductHeadController::class,'store'])->name('product_head.store');

