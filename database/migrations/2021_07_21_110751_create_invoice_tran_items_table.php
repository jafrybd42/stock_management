<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTranItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_tran_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('invoice_tran_id')->constrained('invoice_trans');
            $table->foreignId('product_id')->constrained('products');
            $table->string('tran_type');
            $table->double('qty', 13, 2);
            $table->string('quantity_as');
            $table->double('price_per_rate', 13, 2);
            $table->double('discount', 13, 2);
            $table->boolean('status')->default(1);
            $table->boolean('is_advance_sales')->default(0);
            $table->integer('store_id')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_tran_items');
    }
}
