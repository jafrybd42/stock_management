<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierBalanceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_balance_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('supplier_id')->constrained('suppliers');
            $table->double('last_payable', 10, 2)->default(0);
            $table->double('current_payable', 10, 2)->default(0);
            $table->double('last_receivable', 10, 2)->default(0);
            $table->double('current_receivable', 10, 2)->default(0);
            $table->timestamp('last_update')->useCurrent();
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_balance_histories');
    }
}
