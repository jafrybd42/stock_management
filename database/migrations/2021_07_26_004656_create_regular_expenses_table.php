<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegularExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regular_expenses', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('amount')->default('0');
            $table->date('date');
            $table->integer('month');
            $table->integer('year');
            $table->integer('store_id')->default('1');
            $table->boolean('status')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regular_expenses');
    }
}
