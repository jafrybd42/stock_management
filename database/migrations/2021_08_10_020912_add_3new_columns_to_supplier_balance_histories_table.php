<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add3newColumnsToSupplierBalanceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_balance_histories', function (Blueprint $table) {
            //
            $table->integer('transaction_type');
            $table->double('amount', 10, 2)->default(0);
            $table->string('reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier_balance_histories', function (Blueprint $table) {
            //
            $table->dropColumn('transaction_type');
            $table->dropColumn('amount');
            $table->dropColumn('reason');
        });
    }
}
