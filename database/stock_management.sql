-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2021 at 09:12 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stock_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cash_type` int(11) NOT NULL,
  `amount` double(13,2) NOT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `customer_id`, `supplier_id`, `user_type`, `cash_type`, `amount`, `bank_name`, `check_no`, `date`, `store_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Customer', 1, 100.00, 'Dhaka Bank', 'asfasfa', '2021-08-06', 1, 1, 1, 1, '2021-08-06 04:34:13', '2021-08-06 04:34:13'),
(2, NULL, 1, 'Supplier', 2, 5000.00, 'SBL', 'swqwq', '2021-08-05', 1, 1, 1, 1, '2021-08-06 04:42:12', '2021-08-06 04:42:12');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity_as` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` double(16,2) NOT NULL,
  `selling_price` double(16,2) NOT NULL,
  `transaction_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_allow_advance_sale` int(11) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'customer.jpg',
  `payable` double(10,2) NOT NULL DEFAULT 0.00,
  `receivable` double(10,2) NOT NULL DEFAULT 0.00,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `description`, `contact_no`, `address`, `image`, `payable`, `receivable`, `store_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Akbar', '', '01670461399', '', 'customer.jpg', 0.00, 0.00, 1, 1, 1, 1, '2021-08-06 04:32:42', '2021-08-06 04:32:42'),
(2, 'Mokbul', '', '01678676676', 'ffafa', 'customer.jpg', 0.00, 0.00, 1, 1, 1, 1, '2021-08-06 05:34:12', '2021-08-06 05:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `customer_balance_histories`
--

CREATE TABLE `customer_balance_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `last_payable` double(10,2) NOT NULL DEFAULT 0.00,
  `current_payable` double(10,2) NOT NULL DEFAULT 0.00,
  `last_receivable` double(10,2) NOT NULL DEFAULT 0.00,
  `current_receivable` double(10,2) NOT NULL DEFAULT 0.00,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_expenses`
--

CREATE TABLE `general_expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_expense_months`
--

CREATE TABLE `general_expense_months` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `general_expense_id` bigint(20) UNSIGNED NOT NULL,
  `month` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `investors`
--

CREATE TABLE `investors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'investor.jpg',
  `balance` double(10,2) NOT NULL DEFAULT 0.00,
  `percentage` double(5,2) NOT NULL DEFAULT 0.00,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `investors_balance_histories`
--

CREATE TABLE `investors_balance_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `investor_id` bigint(20) UNSIGNED NOT NULL,
  `current_balance` double(10,2) NOT NULL DEFAULT 0.00,
  `previous_balance` double(10,2) NOT NULL DEFAULT 0.00,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_trans`
--

CREATE TABLE `invoice_trans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `inv_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tran_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` double(13,2) NOT NULL,
  `due` double(13,2) NOT NULL,
  `paid` double(13,2) NOT NULL,
  `discount` double(13,2) NOT NULL,
  `total` double(13,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `is_advance_sales` tinyint(1) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_trans`
--

INSERT INTO `invoice_trans` (`id`, `customer_id`, `supplier_id`, `inv_no`, `tran_type`, `sub_total`, `due`, `paid`, `discount`, `total`, `status`, `is_advance_sales`, `store_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'inv2021', 'sales to customer', 600.00, 0.00, 600.00, 0.00, 600.00, 1, 0, 1, 1, 1, '2021-08-06 04:32:42', '2021-08-06 04:32:42'),
(2, NULL, 1, 'inv2021', 'Supplier Return', 10000.00, 0.00, 10000.00, 0.00, 10000.00, 1, 0, 1, 1, 1, '2021-08-06 04:33:59', '2021-08-06 04:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tran_items`
--

CREATE TABLE `invoice_tran_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_tran_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `tran_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` double(10,2) NOT NULL,
  `quantity_as` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_per_rate` double(10,2) NOT NULL,
  `discount` double(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `is_advance_sales` tinyint(1) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_tran_items`
--

INSERT INTO `invoice_tran_items` (`id`, `invoice_tran_id`, `product_id`, `tran_type`, `qty`, `quantity_as`, `price_per_rate`, `discount`, `status`, `is_advance_sales`, `store_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'sales to customer', 2.00, 'KG', 300.00, 0.00, 1, 0, 1, 1, 1, '2021-08-06 04:32:42', '2021-08-06 04:32:42'),
(2, 2, 2, 'Supplier Return', 100.00, 'KG', 100.00, 0.00, 1, 0, 1, 1, 1, '2021-08-06 04:33:59', '2021-08-06 04:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `journals`
--

CREATE TABLE `journals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tran_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tran_id` int(11) NOT NULL,
  `amount` double(13,2) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `journals`
--

INSERT INTO `journals` (`id`, `name`, `type`, `tran_type`, `description`, `tran_id`, `amount`, `store_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Sales', 'Cr', 'invoice', '', 1, 600.00, 1, 1, 1, 1, '2021-08-06 04:32:42', '2021-08-06 04:32:42'),
(2, 'Cash', 'Dr', 'invoice', '', 1, 600.00, 1, 1, 1, 1, '2021-08-06 04:32:43', '2021-08-06 04:32:43'),
(3, 'Purchase Return', 'Cr', 'invoice', '', 2, 10000.00, 1, 1, 1, 1, '2021-08-06 04:33:59', '2021-08-06 04:33:59'),
(4, 'Cash', 'Dr', 'invoice', '', 2, 10000.00, 1, 1, 1, 1, '2021-08-06 04:34:00', '2021-08-06 04:34:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2021_06_18_113721_create_product_heads_table', 1),
(16, '2021_07_03_005944_create_products_table', 9),
(17, '2021_07_03_012607_create_products_table', 10),
(22, '2021_07_06_013635_create_carts_table', 13),
(35, '2021_06_30_193422_create_partners_table', 14),
(37, '2021_06_30_194548_create_investors_table', 14),
(40, '2021_07_04_225205_create_invoice_trans_table', 14),
(41, '2021_07_04_225435_create_invoice_tran_items_table', 14),
(43, '2021_07_10_013302_create_investors_table', 15),
(44, '2021_07_10_013650_create_partners_table', 15),
(45, '2021_07_10_015138_create_investors_balance_history_table', 16),
(46, '2021_07_10_015211_create_partners_balance_history_table', 16),
(56, '2021_07_21_092407_create_invoice_trans_table', 21),
(57, '2021_07_21_093420_create_invoice_trans_table', 22),
(59, '2021_07_21_095958_create_invoice_tran_items_table', 23),
(121, '2014_10_12_000000_create_users_table', 24),
(122, '2014_10_12_100000_create_password_resets_table', 24),
(123, '2019_08_19_000000_create_failed_jobs_table', 24),
(124, '2021_06_18_115033_create_product_heads_table', 24),
(125, '2021_06_18_203307_create_product_categories_table', 24),
(126, '2021_06_18_211339_create_product_groups_table', 24),
(127, '2021_06_19_142905_create_product_types_table', 24),
(128, '2021_06_29_202352_create_product_sizes_table', 24),
(129, '2021_06_30_020819_create_suppliers_table', 24),
(130, '2021_06_30_112157_create_customers_table', 24),
(131, '2021_06_30_153312_create_products_table', 24),
(132, '2021_06_30_194516_create_expenses_table', 24),
(133, '2021_07_03_013806_create_stocks_table', 24),
(134, '2021_07_03_132938_create_sales_table', 24),
(135, '2021_07_06_030339_create_carts_table', 24),
(136, '2021_07_10_033802_create_investors_table', 24),
(137, '2021_07_10_033841_create_partners_table', 24),
(138, '2021_07_10_033940_create_partners_balance_histories_table', 24),
(139, '2021_07_10_034001_create_investors_balance_histories_table', 24),
(140, '2021_07_15_110009_create_stock_history_table', 24),
(141, '2021_07_15_145335_create_customer_balance_histories_table', 24),
(142, '2021_07_15_145412_create_supplier_balance_histories_table', 24),
(143, '2021_07_21_095917_create_invoice_trans_table', 24),
(144, '2021_07_21_110751_create_invoice_tran_items_table', 24),
(145, '2021_07_26_003311_create_general_expenses_table', 24),
(146, '2021_07_26_004147_create_general_expense_months_table', 24),
(147, '2021_07_26_004656_create_regular_expenses_table', 24),
(148, '2021_07_29_163159_create_journals_table', 24),
(149, '2021_08_06_095301_create_banks_table', 24);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'partner.jpg',
  `balance` double(10,2) NOT NULL DEFAULT 0.00,
  `percentage` double(5,2) NOT NULL DEFAULT 0.00,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partners_balance_histories`
--

CREATE TABLE `partners_balance_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_id` bigint(20) UNSIGNED NOT NULL,
  `current_balance` double(10,2) NOT NULL DEFAULT 0.00,
  `previous_balance` double(10,2) NOT NULL DEFAULT 0.00,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_head_id` bigint(20) UNSIGNED NOT NULL,
  `product_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_size_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `quatity_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_head_id`, `product_category_id`, `product_type_id`, `product_size_id`, `product_group_id`, `quantity`, `quatity_value`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 'KG/TON', '0', 1, 1, 1, '2021-08-06 04:31:17', '2021-08-06 04:31:17'),
(2, 2, 1, 1, 1, 1, 'KG/TON', '0', 1, 1, 1, '2021-08-06 04:31:33', '2021-08-06 04:31:33');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `title`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'construction', 1, 1, 1, '2021-08-06 04:30:35', '2021-08-06 04:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `product_groups`
--

CREATE TABLE `product_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_groups`
--

INSERT INTO `product_groups` (`id`, `group_name`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '3*3', 1, 1, 1, '2021-08-06 04:30:50', '2021-08-06 04:30:50');

-- --------------------------------------------------------

--
-- Table structure for table `product_heads`
--

CREATE TABLE `product_heads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_heads`
--

INSERT INTO `product_heads` (`id`, `title`, `store_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Rod', 1, 1, '2021-08-06 04:30:21', '2021-08-06 04:30:21'),
(2, 'Cement', 1, 1, '2021-08-06 04:30:28', '2021-08-06 04:30:28');

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`id`, `title`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Large', 1, 1, 1, '2021-08-06 04:31:00', '2021-08-06 04:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `title`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'XX', 1, 1, 1, '2021-08-06 04:30:43', '2021-08-06 04:30:43');

-- --------------------------------------------------------

--
-- Table structure for table `regular_expenses`
--

CREATE TABLE `regular_expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `current_stock` int(11) NOT NULL DEFAULT 0,
  `buying_price` double(10,2) NOT NULL DEFAULT 0.00,
  `selling_price` double(10,2) NOT NULL DEFAULT 0.00,
  `average_price` double(10,2) NOT NULL DEFAULT 0.00,
  `reorder_level` int(11) NOT NULL DEFAULT 0,
  `last_tran_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `product_id`, `current_stock`, `buying_price`, `selling_price`, `average_price`, `reorder_level`, `last_tran_date`, `store_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1998, 200.00, 300.00, 0.00, 0, '2021-08-06 04:31:44', 1, 1, 1, '2021-08-06 04:31:17', '2021-08-06 04:32:42'),
(2, 2, 100, 100.00, 350.00, 0.00, 0, '2021-08-06 04:32:17', 1, 1, 1, '2021-08-06 04:31:34', '2021-08-06 04:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `stock_history`
--

CREATE TABLE `stock_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stock_id` bigint(20) UNSIGNED NOT NULL,
  `previous_stock` int(11) NOT NULL DEFAULT 0,
  `buying_price` double(10,2) NOT NULL DEFAULT 0.00,
  `selling_price` double(10,2) NOT NULL DEFAULT 0.00,
  `average_price` double(10,2) NOT NULL DEFAULT 0.00,
  `reorder_level` int(11) NOT NULL DEFAULT 0,
  `last_tran_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_update` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'supplier.jpg',
  `payable` double(10,2) NOT NULL DEFAULT 0.00,
  `receivable` double(10,2) NOT NULL DEFAULT 0.00,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `company_name`, `contact_no`, `address`, `image`, `payable`, `receivable`, `store_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Rahim', '', '01745777777', '', 'supplier.jpg', 0.00, 0.00, 1, 1, 1, 1, '2021-08-06 04:33:59', '2021-08-06 04:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_balance_histories`
--

CREATE TABLE `supplier_balance_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `last_payable` double(10,2) NOT NULL DEFAULT 0.00,
  `current_payable` double(10,2) NOT NULL DEFAULT 0.00,
  `last_receivable` double(10,2) NOT NULL DEFAULT 0.00,
  `current_receivable` double(10,2) NOT NULL DEFAULT 0.00,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 1,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'super',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `store_id`, `user_type`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shovon', 'stock@gmail.com', '$2y$10$2YYU5G.M5qAOMP6XRLpwXeSjocAPbvjiNR9jeWeBS25P2PAg6Szmq', 1, 'super', 1, NULL, '2021-08-06 04:30:01', '2021-08-06 04:30:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banks_customer_id_foreign` (`customer_id`),
  ADD KEY `banks_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_balance_histories`
--
ALTER TABLE `customer_balance_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_balance_histories_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `general_expenses`
--
ALTER TABLE `general_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_expense_months`
--
ALTER TABLE `general_expense_months`
  ADD PRIMARY KEY (`id`),
  ADD KEY `general_expense_months_general_expense_id_foreign` (`general_expense_id`);

--
-- Indexes for table `investors`
--
ALTER TABLE `investors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investors_balance_histories`
--
ALTER TABLE `investors_balance_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `investors_balance_histories_investor_id_foreign` (`investor_id`);

--
-- Indexes for table `invoice_trans`
--
ALTER TABLE `invoice_trans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_trans_customer_id_foreign` (`customer_id`),
  ADD KEY `invoice_trans_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `invoice_tran_items`
--
ALTER TABLE `invoice_tran_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_tran_items_invoice_tran_id_foreign` (`invoice_tran_id`),
  ADD KEY `invoice_tran_items_product_id_foreign` (`product_id`);

--
-- Indexes for table `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners_balance_histories`
--
ALTER TABLE `partners_balance_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `partners_balance_histories_partner_id_foreign` (`partner_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_product_head_id_foreign` (`product_head_id`),
  ADD KEY `products_product_category_id_foreign` (`product_category_id`),
  ADD KEY `products_product_type_id_foreign` (`product_type_id`),
  ADD KEY `products_product_size_id_foreign` (`product_size_id`),
  ADD KEY `products_product_group_id_foreign` (`product_group_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_groups`
--
ALTER TABLE `product_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_heads`
--
ALTER TABLE `product_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regular_expenses`
--
ALTER TABLE `regular_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stocks_product_id_foreign` (`product_id`);

--
-- Indexes for table `stock_history`
--
ALTER TABLE `stock_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_history_stock_id_foreign` (`stock_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_balance_histories`
--
ALTER TABLE `supplier_balance_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_balance_histories_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_balance_histories`
--
ALTER TABLE `customer_balance_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_expenses`
--
ALTER TABLE `general_expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_expense_months`
--
ALTER TABLE `general_expense_months`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investors`
--
ALTER TABLE `investors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investors_balance_histories`
--
ALTER TABLE `investors_balance_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_trans`
--
ALTER TABLE `invoice_trans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice_tran_items`
--
ALTER TABLE `invoice_tran_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `journals`
--
ALTER TABLE `journals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partners_balance_histories`
--
ALTER TABLE `partners_balance_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_groups`
--
ALTER TABLE `product_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_heads`
--
ALTER TABLE `product_heads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `regular_expenses`
--
ALTER TABLE `regular_expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock_history`
--
ALTER TABLE `stock_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supplier_balance_histories`
--
ALTER TABLE `supplier_balance_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `banks_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`);

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `customer_balance_histories`
--
ALTER TABLE `customer_balance_histories`
  ADD CONSTRAINT `customer_balance_histories_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `general_expense_months`
--
ALTER TABLE `general_expense_months`
  ADD CONSTRAINT `general_expense_months_general_expense_id_foreign` FOREIGN KEY (`general_expense_id`) REFERENCES `general_expenses` (`id`);

--
-- Constraints for table `investors_balance_histories`
--
ALTER TABLE `investors_balance_histories`
  ADD CONSTRAINT `investors_balance_histories_investor_id_foreign` FOREIGN KEY (`investor_id`) REFERENCES `investors` (`id`);

--
-- Constraints for table `invoice_trans`
--
ALTER TABLE `invoice_trans`
  ADD CONSTRAINT `invoice_trans_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `invoice_trans_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`);

--
-- Constraints for table `invoice_tran_items`
--
ALTER TABLE `invoice_tran_items`
  ADD CONSTRAINT `invoice_tran_items_invoice_tran_id_foreign` FOREIGN KEY (`invoice_tran_id`) REFERENCES `invoice_trans` (`id`),
  ADD CONSTRAINT `invoice_tran_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `partners_balance_histories`
--
ALTER TABLE `partners_balance_histories`
  ADD CONSTRAINT `partners_balance_histories_partner_id_foreign` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_product_category_id_foreign` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`),
  ADD CONSTRAINT `products_product_group_id_foreign` FOREIGN KEY (`product_group_id`) REFERENCES `product_groups` (`id`),
  ADD CONSTRAINT `products_product_head_id_foreign` FOREIGN KEY (`product_head_id`) REFERENCES `product_heads` (`id`),
  ADD CONSTRAINT `products_product_size_id_foreign` FOREIGN KEY (`product_size_id`) REFERENCES `product_sizes` (`id`),
  ADD CONSTRAINT `products_product_type_id_foreign` FOREIGN KEY (`product_type_id`) REFERENCES `product_types` (`id`);

--
-- Constraints for table `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `stock_history`
--
ALTER TABLE `stock_history`
  ADD CONSTRAINT `stock_history_stock_id_foreign` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`);

--
-- Constraints for table `supplier_balance_histories`
--
ALTER TABLE `supplier_balance_histories`
  ADD CONSTRAINT `supplier_balance_histories_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
