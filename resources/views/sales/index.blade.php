@extends('layouts.app')
@section('title', 'Product Head')
@push('css')


@endpush

@section('content')


<div class="main-content">
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Product List</h4>
                    </div>
                    <div class="card-body">
                        <div class="owl-carousel owl-theme" id="products-carousel">

                            <div class="row">
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="product_head">
                                            <option value="0">Select Head</option>
                                            @foreach ($product_head_list as $key => $head)
                                            <option value="{{ $head->id }}">{{ $head->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="product_size">
                                            <option value="0">Select Size</option>
                                            @foreach ($product_size_list as $key => $size)
                                            <option value="{{ $size->id }}">{{ $size->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="product_group">
                                            <option value="0">Select Group</option>
                                            @foreach ($product_group_list as $key => $group)
                                            <option value="{{ $group->id }}">{{ $group->group_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="product_type">
                                            <option value="0">Select Type</option>
                                            @foreach ($product_type_list as $key => $type)
                                            <option value="{{ $type ->id }}">{{ $type->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="product_category">
                                            <option value="0">Select Category</option>
                                            @foreach ($product_cat_list as $key => $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <input type="button" id="update-product-list" value="Search">
                            </div>

                        </div>

                        <!-- {{ method_field('POST') }}
                                {{ csrf_field() }} -->

                        <!-- <button type="button" class="btn btn-success">
                                                Add
                                            </button> -->

                        <div class="table-responsive">
                            <table id="example2" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>

                                        <th>Head</th>
                                        <th>Category</th>
                                        <th>Type</th>
                                        <th>Size</th>
                                        <th>Group</th>
                                        <th>Qty</th>
                                        <th>Stock</th>
                                        <th>Selling Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <div id="tableBodyDiv">
                                    <tbody id="tableBody">
                                        @foreach ($product_list as $key => $product)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>
                                                {{ $product->product_head->title }}
                                            </td>
                                            <td>
                                                @if ($product->product_category_id == 0)
                                                None
                                                @else
                                                {{ $product->product_category->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($product->product_type_id == 0)
                                                None
                                                @else
                                                {{ $product->product_type->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($product->product_size_id == 0)
                                                None
                                                @else
                                                {{ $product->product_size->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($product->product_group_id == 0)
                                                None
                                                @else
                                                {{ $product->product_group->group_name }}
                                                @endif
                                            </td>

                                            <td>{{ $product->quantity }}</td>

                                            <td>
                                                {{ $product->stock }}
                                            </td>

                                            <td>
                                                {{ $product->selling_price }} TK
                                            </td>

                                            <td>
                                                <button class="btn btn-primary" onclick="changeModalData( '{{ $product }}')" data-toggle="modal" data-target="#modal-part">
                                                    + Add To List
                                                </button>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    {{-- ADD Modal --}}
    <div class="modal fade" role="dialog" id="modal-part">
        <div class="modal-dialog" role="document">
            {{-- {{route('category.store')}} --}}
            <!-- <form method="POST" action="{{ route('investor.store') }}" enctype="multipart/form-data"> -->
            {{ csrf_field() }}
            <div class=" modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Product To Cart</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group">
                        <label>Product Name <code>*</code></label>
                        <input type="text" name="name" id="product_name" class="form-control form-control-lg" readonly />
                    </div> --}}

                    <input type="hidden" name="name" id="product_id" class="form-control form-control-lg" readonly />

                    <div class="form-group">
                        <label>Qty As <code>*</code></label>
                        <select name="qty_as" id="qty_as" class="form-control form-control-lg">
                        </select>
                        <!-- <input type="text" name="qty_as" id="qty_as" class="form-control form-control-lg" readonly /> -->
                    </div>
                    <div class="form-group">
                        <label>Qty <code>*</code></label>
                        <input type="number" name="qty" id="qty" class="form-control form-control-lg" />
                    </div>
                    <div class="form-group">
                        <label>Price <code></code></label>
                        <input type="number" name="price" id="price" class="form-control form-control-lg" readonly />
                    </div>

                    <div class="form-group">
                        <label>Sales Type <code>*</code></label>
                        <select name="sales_type" id="sales_type" class="form-control form-control-lg">
                        </select>
                    </div>

                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="sale-add-to-cart" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
            <!-- </form> -->
        </div>
    </div>

</div>

@endsection

@section('extra-js')

<!-- <script type="text/javascript">
    $(document).ready(function() {

        function showmodal(myvalue) {
            console.log(myvalue);

            $('#title').val($('#t' + id).html());
            console.log("hi");

            var x = $('#myform').attr('action', '{{ URL::to("/investor") }}/' + id);


            $('#myModal2').modal({
                backdrop: 'static'
            });
        }

    });
</script> -->


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script>
    $(document).on('click', '#sale-add-to-cart', function(e) {

        var product_id = document.getElementById("product_id").value;
        var qty = document.getElementById("qty").value;
        var qty_as = document.getElementById("qty_as").value;
        var sales_type = document.getElementById("sales_type").value;

        $.ajax({
            // url: '/sales/addToCart/',
            url: "{{route('sales.add_to_cart')}}",
            type: 'POST',
            dataType: "json",
            data: {
                "_token": "{{ csrf_token() }}",
                'product_id': product_id,
                'qty': qty,
                'qty_as': qty_as,
                'sales_type': sales_type
            },

            success: function(response) { // console.log("successful");
                // $('#tbody_teacher_assign_subject').html(response);
                response = JSON.parse(response);

                console.log(response);

                if (response.success == true) {
                    $('#modal-part').modal('hide');
                    // console.log(response);
                    alert("Added to cart");
                } else {
                    alert(response.message);
                }

            },
            error: function(xhr, textStatus, errorThrown) {
                // console.log("Fail");
                // console.log(textStatus);
                // console.log(errorThrown);
                // console.log(xhr);
                alert("Fail");
            }

        });
    });
</script>

<script>
    $(document).on('click', '#update-product-list', function(e) {

        var product_head_id = document.getElementById("product_head").value;
        var product_size_id = document.getElementById("product_size").value;
        var product_category_id = document.getElementById("product_category").value;
        var product_type_id = document.getElementById("product_type").value;
        var product_group_id = document.getElementById("product_group").value;

        $.ajax({
            url: "{{route('product.search')}}",
            type: 'POST',
            dataType: 'html',
            data: {
                "_token": "{{ csrf_token() }}",
                'product_head_id': product_head_id,
                'product_size_id': product_size_id,
                'product_category_id': product_category_id,
                'product_type_id': product_type_id,
                'product_group_id': product_group_id,
                'tranType': "sales",
            },

            success: function(response) {
                // console.log(response);

                $('#example2 tbody').remove();
                // $('#tableBody').html(response);
                $('#example2').append(response);

            },
            error: function(xhr, textStatus, errorThrown) {
                // console.log("Fail");
                // console.log(textStatus);
                // console.log(errorThrown);
                // console.log(xhr);
                alert("Fail");
            }

        });
    });
</script>

<script>
    function changeModalData(product_obj) {
        // console.log("ddddddddddddd");
        // console.log(JSON.parse(product_obj));
        product_obj = JSON.parse(product_obj);

        $('#qty_as option').remove();
        $('#sales_type option').remove();

        if (product_obj.quantity.toUpperCase() == ('KG/TON').toUpperCase()) {
            $('#qty_as').append('<option value="KG" selected>KG</option>');
            $('#qty_as').append('<option value="TON">TON</option>');
        } else {
            $('#qty_as').append('<option value="PCS" selected>Pcs</option>');
            $('#qty_as').append('<option value="BOX">BOX</option>');
        }

        $('#sales_type').append('<option value="0" selected>Regular Sales</option>');
        $('#sales_type').append('<option value="1">Advance Sales</option>');


        // document.getElementById("qty_as").value = product_obj.quantity;
        document.getElementById("qty").value = 0;
        document.getElementById("price").value = product_obj.selling_price;
        document.getElementById("product_id").value = product_obj.id;
        document.getElementById("product_name").value = "Test";
    }
</script>

<script>
    function changeModalDataByID(product_id = 0) {

        $.ajax({
            url: "{{route('product.detailsByID')}}",
            type: 'POST',
            dataType: 'json',
            data: {
                "_token": "{{ csrf_token() }}",
                'product_id': product_id,
            },

            success: function(response) {

                // response = JSON.parse(response);
                // console.log(response);

                if (response.success == true) {
                    // document.getElementById("qty_as").value = response.data.quantity;

                    $('#qty_as option').remove();

                    if (response.data.quantity.toUpperCase() == ('KG/TON').toUpperCase()) {
                        $('#qty_as').append('<option value="KG" selected>KG</option>');
                        $('#qty_as').append('<option value="TON">TON</option>');
                    } else {
                        $('#qty_as').append('<option value="PCS" selected>Pcs</option>');
                        $('#qty_as').append('<option value="BOX">BOX</option>');
                    }

                    document.getElementById("qty").value = 0;
                    document.getElementById("price").value = response.data.selling_price;
                    document.getElementById("product_id").value = response.data.id;
                    document.getElementById("product_name").value = "Test";
                } else {
                    $('#modal-part').modal('hide');
                }


            },
            error: function(xhr, textStatus, errorThrown) {
                // console.log("Fail");
                // console.log(textStatus);
                // console.log(errorThrown);
                // console.log(xhr);
                // alert("Fail");
            }

        });
    }
</script>
<script>
    $(document).ready(function() {
        var table = $("#example2").DataTable({
            searching: false,
            lengthChange: false,
            buttons: true,
            buttons: ["copy", "excel", "pdf", "print"],
            pagging: true,
            ordering: true
        });
        table
            .buttons()
            .container()
            .appendTo("#example_wrapper .col-md-6:eq(0)");
    });
</script>
@endsection