@extends('layouts.app')
@section('title', 'Product Head')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Product Head</h1>
            </div>


            <div class="col-6 d-flex flex-row-reverse">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                    + Add New
                </button>
            </div>
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($product_heads as $key => $product_head)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td id="t{{ $product_head->id }}">{{ $product_head->title }}</td>

                                        <td>


                                            <a href="{{ route('product-head.edit', $product_head->id) }}" type="button" class="btn btn-primary edit">Edit</a>


                                            {{-- <a class="btn btn-primary" onclick="changeModalData( '{{ $product_head }}')" data-toggle="modal" data-target="#modal-part2">
                                            + Check Modal
                                            </a> --}}


                                            <form method="POST" action="{{ route('product-head.destroy', $product_head->id) }}" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                </button>
                                            </form>
                                        </td>



                                    </tr>
                                    @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Modal -->
    {{-- ADD Modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
        <div class="modal-dialog" role="document">
            {{-- {{route('category.store')}} --}}
            <form method="POST" action="{{ route('product-head.store') }}">
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Product Head</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Title <code>*</code></label>
                            <input type="text" name="title" class="form-control form-control-lg" />
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- End -->
    {{-- EDIT MODAL --}}
    {{-- {{URL::to('/product-head')}} --}}
    <div class="modal fade" id="modal-part2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            {{-- <form method="POST" action="" id="editForm">
                    {{ method_field('PATCH') }}
            {{ csrf_field() }} --}}
            <div class=" modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Product Head</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Title <code>*</code></label>
                        <input type="text" name="title" id="title" class="form-control form-control-lg" />
                    </div>
                </div>
                <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly />
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="editProductHead" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
            {{-- </form> --}}
        </div>
    </div>
</div>

@endsection

@section('extra-js')

<script>
    $(document).on('click', '#editProductHead', function(e) {


        var id = document.getElementById("id").value;
        var title = document.getElementById("title").value;



        $.ajax({

            url: "{{route('product-head.modalEdit')}}",
            type: 'POST',
            dataType: "json",
            data: {
                "_token": "{{ csrf_token() }}",
                'id': id,
                'title': title,
            },



            success: function(response) {

                // console.log("successful");
                // $('#tbody_teacher_assign_subject').html(response);
                response = JSON.parse(response);

                if (response.success == true) {

                    // var responsedata = $.parseJSON(response);

                    $('#modal-part2').modal('hide');

                    console.log(response.data);
                    $("#example").html(response.data);
                    // console.log(data);

                    // alert("success");
                } else {
                    alert(response.message);
                }

            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
                console.log(textStatus);
                console.log(errorThrown);
                console.log(xhr);
                alert("Fail");
            }

        });
    });
</script>
<script>
    function changeModalData(product_obj) {

        console.log((product_obj));
        product_obj = JSON.parse(product_obj);

        document.getElementById("id").value = product_obj.id;
        document.getElementById("title").value = product_obj.title;
        //console.log(product_obj.id);

    }
</script>
@endsection

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->