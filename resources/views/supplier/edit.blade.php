@extends('layouts.app')
@section('title', 'Product size')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Product Size</h1>
            </div>



        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-6">
                <div class="card">
                    <div class="card-body p-0">
                        <form method="POST" action="{{route('supplier.update',$id)}}" id="editForm" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                            <div class=" modal-content">
                                <div class="modal-header">
                                </div>
                                <div class="modal-body">
                                    <label>Name <code>*</code></label>
                                    <input type="text" name="name" id="name" value="{{ $supplier_info->name }}" class="form-control form-control-lg" />
                                </div>

                                <div class="modal-body">
                                    <label>Phone <code>*</code></label>
                                    <input type="text" name="phone" id="phone" value="{{ $supplier_info->contact_no }}" class="form-control form-control-lg" />
                                </div>


                                <div class="modal-body">
                                    <label>Address <code></code></label>
                                    <input type="text" name="address" id="address" value="{{ $supplier_info->address }}" class="form-control form-control-lg" />
                                </div>


                                <div class="modal-body">
                                    <label>Company <code></code></label>
                                    <input type="text" name="company" id="company" value="{{ $supplier_info->company_name }}" class="form-control form-control-lg" />
                                </div>


                                <div class="modal-body">
                                    <label>Photo <code></code></label>
                                    <input type="file" name="photo" class="form-control form-control-lg" />
                                </div>

                                <input type="hidden" name="id" id="id" value="{{ $supplier_info->id }}" class="form-control form-control-lg" />
                                <div class="modal-footer bg-whitesmoke br">

                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Modal -->
    {{-- ADD Modal --}}

</div>

@endsection
@section('extra-js')

@endsection

