@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Journal List</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
                <h1> ( <code>Dr: {{ $main_dr }} -- Cr {{ $main_cr }}</code>)</h1>
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">

                        <div class="row">
                            @foreach ($mainJournalList as $key => $mainJournal)
                            <div class="col-lg-6 table-responsive p-5">
                                <h1 class="pb-3">{{ $mainJournal['name']}}</h1>

                                <?php $main = 0; ?>

                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr class="thh2">
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Dr</th>
                                            <th>Cr</th>
                                        </tr>
                                    </thead>
                                    <tbody class="thh2">

                                        @foreach($mainJournal['data'] as $key2 => $journal)

                                        <tr>
                                            <td> # </td>
                                            <td> {{ $journal->name  }} </td>
                                            @if( $journal->type == "Cr")
                                            <!-- <td> </td> -->

                                            <?php $main -= $journal->amount; ?>
                                            <td class="margin-check"> {{ $journal->amount  }} </td>
                                            @else

                                            <?php $main += $journal->amount; ?>

                                            <td class="margin-check"> {{ $journal->amount  }} </td>

                                            @endif

                                        </tr>
                                        <tr class="thh2">

                                            @if( $main > 0 )

                                            <td class="totalNew"> </td>
                                            <td class="totalNew"> </td>
                                            <td class="totalNew"> {{ abs($main)  }} </td>
                                            <td class="totalNew"> </td>

                                            @else

                                            <td class="totalNew"> </td>
                                            <td class="totalNew"> </td>
                                            <!-- <td class="totalNew"> </td> -->
                                            <td class="totalNew"> {{abs($main) }} </td>
                                            <td class="totalNew"> </td>

                                            @endif

                                        </tr>

                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            @endforeach
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')


    @endsection