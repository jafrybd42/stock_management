@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Owner Equity</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-5">

                        <div class="table-responsive">
                            <table id="example33" class="table table-striped">
                                <thead>
                                    <tr class="thh2">
                                        <th>#</th>
                                        <th>Details A/C</th>
                                        <th>TK</th>
                                    </tr>
                                </thead>
                                <tbody class="thh2">
                                    <tr>
                                        <td> 1 </td>
                                        <td> Capital </td>
                                        <td> {{ $capital  }} </td>
                                    </tr>

                                    <tr>
                                        <td> 2 </td>
                                        <td> (+) Net Profit </td>
                                        <td class="margin-check"> {{ $netProfite  }} </td>
                                    </tr>


                                    <tr>
                                        <td> </td>
                                        <td> </td>
                                        <td> {{ $capital  + $netProfite  }} </td>
                                    </tr>

                                    <tr>
                                        <td> 3 </td>
                                        <td> (-) Withdraw </td>
                                        <td class="margin-check"> {{ $withdraw  }} </td>
                                    </tr>

                                    <tr class="thh2">
                                        <th class="totalNew"></th>
                                        <th class="totalNew"> <b> Net Capital </b> </th>
                                        <th class="">
                                            <b>
                                                {{ $capital  + $netProfite -$withdraw  }} TK
                                            </b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')

    <script>
        // $(document).ready(function() {
        //     var table = $('#example').DataTable({
        //         "pageLength": 25
        //     });
        // });
    </script>
    @endsection