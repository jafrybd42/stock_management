@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Trial Balance </h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-5">
                        <div class="table-responsive">
                            <table id="example33" class="table table-striped">
                                <thead>
                                    <tr class="thh">
                                        <th width="50px">#</th>
                                        <th>Details A/C</th>
                                        <th>Dr</th>
                                        <th>Cr</th>
                                    </tr>
                                </thead>
                                <tbody class="thh">
                                    @foreach ($trialBalanceEntityData as $key => $entityData)
                                    <tr>
                                        <td> {{$key + 1 }} </td>
                                        <td> {{ $entityData['name']  }} </td>

                                        @if( $entityData['type'] == "Cr" )
                                        <td class="notavb"> </td>
                                        <td class="avb"> {{ $entityData['amount']  }} </td>

                                        @else

                                        <td class="avb"> {{ $entityData['amount'] }} </td>
                                        <td class="notavb"> </td>
                                        @endif

                                    </tr>
                                    @endforeach


                                    <tr>
                                        <td colspan="2"> Total </td>
                                        <td> {{ $main_dr  }} </td>
                                        <td> {{ $main_cr  }} </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')

    <script>
        // $(document).ready(function() {
        //     var table = $('#example').DataTable({
        //         "pageLength": 25
        //     });
        // });
    </script>
    @endsection