@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Balance Sheet</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-5">
                        <div class="row">
                            <div class="col-lg-6 table-responsive">
                                <h5 class="px-2"># Assets</h5>
                                <table id="example33" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Details A/C</th>
                                            <th>TK</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ------  Assets ----- -->
                                        <!-- <tr>
                                            <td> Assets </td>
                                            <td> </td>
                                        </tr> -->

                                        @foreach($balanceSheetProperty['Assets'] as $key2 => $journal)

                                        <tr>
                                            <td> {{ $journal['name']  }} </td>
                                            <td> {{ $journal['amount']  }} </td>
                                        </tr>

                                        @endforeach

                                        <tr>
                                            <td class="totalNew"> <b> Total Current Assets </b> </td>
                                            <!-- <td>
                                                <hr>
                                            </td> -->
                                            <td class="totalNew"> {{ $total_assets }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 table-responsive">
                                <h5 class="px-2"># Fixed Assets </h5>
                                <table id="example33" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Details A/C</th>
                                            <th>TK</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ------  Fixed Assets ----- -->

                                        @foreach($balanceSheetProperty['Fixed Assets'] as $key2 => $journal)

                                        <tr>
                                            <td> {{ $journal['name']  }} </td>
                                            <td> {{ $journal['amount']  }} </td>
                                        </tr>

                                        @endforeach

                                        <tr>
                                            <td class="totalNew"> <b> Total Fixed Assets </b> </td>
                                            <td class="totalNew"> {{ $total_fixed_assets }} </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 table-responsive">
                                <h5 class="px-2 mt-3"># Liability and Owner Equity </h5>
                                <table id="example33" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Details A/C</th>
                                            <th>TK</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ------  Liability  --- -->

                                        @foreach($balanceSheetProperty['Liability'] as $key2 => $journal)

                                        <tr>
                                            <td> {{ $journal['name']  }} </td>
                                            <td> {{ $journal['amount']  }} </td>
                                        </tr>

                                        @endforeach

                                        <tr>
                                            <td class="totalNew"> <b> Total Liability and Owner Equity </b> </td>
                                            <td class="totalNew"> {{ $total_liability }} </td>
                                        </tr>




                                        <!-- <tr>
                                        <td>  </td>
                                        <td> <b>  <h5> Net Capital  </h5> </b> </td>
                                        <td>  </td>
                                        <td> <b> <h5>  TK </h5>  </b> </td>
                                    </tr> -->


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')

    <script>
        // $(document).ready(function() {
        //     var table = $('#example').DataTable({
        //         "pageLength": 25
        //     });
        // });
    </script>
    @endsection