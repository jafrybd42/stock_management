@extends('layouts.app')

@section('extra-css')
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Details of Bank Data</h1>
                </div>



            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body p-0">

                            <div class=" modal-content">

                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>User Type <code>*</code></label>
                                        <input type="text" name="user_type" id="user_type"
                                            value="{{ $bankData->user_type }}" class="form-control form-control-lg"
                                            readonly />
                                    </div>

                                    <div class="form-group">
                                        <label>User Name <code>*</code></label>
                                        <input type="text" name="user_name" id="user_name"

                                        @if ($bankData->user_type == "Customer") value="{{ $bankData->customer->name }}"
                                        @elseif($bankData->user_type == "Supplier") value="{{ $bankData->supplier->name }}"
                                        @elseif($bankData->user_type == "Partner") value="{{ $bankData->partner->name }}"
                                        @elseif($bankData->user_type == "Investor") value="{{ $bankData->investor->name }}"
                                        @endif class="form-control form-control-lg"
                                            readonly />
                                    </div>

                                    <div class="form-group">
                                        <label>Transaction Type <code>*</code></label>
                                        <input type="text" name="user_type" id="user_type" @if ($bankData->cash_type == 1) value="Payable"
                                        @elseif($bankData->cash_type == 2)
                                            value="Receivable" @endif class="form-control form-control-lg"
                                            readonly />
                                    </div>



                                    <div class="form-group">
                                        <label>Bank Name <code>*</code></label>
                                        <input type="text" name="bank_name" id="bank_name"
                                            class="form-control form-control-lg" value="{{ $bankData->bank_name }}"
                                            readonly />
                                    </div>

                                    <div class="form-group">
                                        <label>Check No <code>*</code></label>
                                        <input type="text" name="check_no" id="check_no"
                                            class="form-control form-control-lg" value="{{ $bankData->check_no }}"
                                            readonly />
                                    </div>

                                    <div class="form-group">
                                        <label>Amount <code>*</code></label>
                                        <input type="number" name="amount" id="amount" class="form-control form-control-lg"
                                            value="{{ $bankData->amount }}" readonly />
                                    </div>
                                    <div class="form-group">
                                        <label>Date <code>*</code></label>
                                        <input type="date" name="date" class="form-control form-control-lg"
                                            value="{{ $bankData->date }}" readonly />
                                    </div>
                                </div>

                                <div class="modal-footer bg-whitesmoke br">


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- Modal -->
        {{-- ADD Modal --}}

    </div>

@endsection


@section('extra-js')

    <script>
        $(document).ready(function() {
            $(".js-example").select2({
                width: 'resolve'
            })
        });
    </script>

@endsection
