@extends('layouts.app')
@section('extra-css')
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Banks Data</h1>
                </div>


                <div class="col-6 d-flex flex-row-reverse">
                    <a href="{{ route('bank.create') }}" type="button" class="btn btn-primary edit">+ Add New</a>


                </div>
            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>User Type</th>
                                            <th>Name</th>
                                            <th>Cash Type</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($banksData as $key => $data)
                                            <tr>

                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $data->user_type }}</td>
                                                <td>
                                                    @if ($data->user_type == 'Customer')
                                                        {{ $data->customer->name }}
                                                    @elseif($data->user_type == "Supplier")
                                                        {{ $data->supplier->name }}

                                                    @elseif($data->user_type == "Partner")
                                                        {{ $data->partner->name }}

                                                    @elseif($data->user_type == "Investor")
                                                        {{ $data->investor->name }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($data->cash_type == 1)
                                                        Payable
                                                    @elseif($data->cash_type == 2)
                                                        Receivable
                                                    @endif
                                                </td>
                                                <td>{{ $data->amount }}</td>
                                                <td>{{ $data->date }}</td>

                                                <td>
                                                    <a href="{{ route('bank.show', $data->id) }}" type="button" class="btn btn-primary edit">Details</a>
                                                    {{-- <a href="{{ route('bank.edit', $data->id) }}" type="button" class="btn btn-primary edit">Edit</a>  --}}


                                            <form method="POST" action="{{ route('bank.destroy', $data->id) }}" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                </button>
                                            </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>


    </div>

@endsection


@section('extra-js')

    <script>
        $(document).ready(function() {
            $(".js-example").select2({
                width: 'resolve'
            })
        });
    </script>

@endsection
