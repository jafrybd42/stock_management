<tbody>
    @foreach ($invoices as $key => $invoice)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td id="t{{ $invoice->id }}">{{ $invoice->inv_no }}</td>
            <td>{{ $invoice->tran_type }}</td>
            <td>{{ $invoice->sub_total }}</td>
            <td>{{ $invoice->discount }}</td>
            <td>{{ $invoice->total }}</td>
            <td>{{ $invoice->due }}</td>
            <td>{{ $invoice->paid }}</td>

           

            <td>{{ $invoice->created_at }}</td>

            <td>

                <a href="{{ route('invoice.details', $invoice->id) }}" type="button"
                    class="btn btn-primary edit">
                    Details
                </a>

                {{-- <a href="{{ route('invoice.generatePdf', $invoice->id) }}" type="button"
                    class="btn btn-primary edit">
                    PDF
                </a> --}}
            </td>

        </tr>
    @endforeach

