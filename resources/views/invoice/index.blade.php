@extends('layouts.app')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Invoice List </h1>
            </div>
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="">
                <div class="card">
                    <div class="card-body p-2">
                        <div class="owl-carousel owl-theme" id="products-carousel">

                            <div class="row pt-4">
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="filter_transaction_type">
                                            <option value="0">Select Transaction Type</option>

                                            <option value="sales to customer">sales to customer</option>
                                            <option value="Supplier Return">Supplier Return</option>
                                            <option value="Supplier Receive">Supplier Receive</option>
                                            <option value="Customer Return">Customer Return</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <input class="form-control" type="text" name="daterange" id="filter_date_range" />
                                    </div>
                                </div>

                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <input class="btn btn-primary" type="button" id="filter" value="Search">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="table-responsive">
                            <table id="example2" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Inv No</th>
                                        <th>Tran Type</th>
                                        <th>Sub Total</th>
                                        <th>Discount</th>
                                        <th>Total</th>
                                        <th>Due</th>
                                        <th>Paid</th>

                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($invoice_tran_list as $key => $invoice)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td id="t{{ $invoice->id }}">{{ $invoice->inv_no }}</td>
                                        <td>{{ $invoice->tran_type }}</td>
                                        <td>{{ $invoice->sub_total }}</td>
                                        <td>{{ $invoice->discount }}</td>
                                        <td>{{ $invoice->total }}</td>
                                        <td>{{ $invoice->due }}</td>
                                        <td>{{ $invoice->paid }}</td>



                                        <td>{{ $invoice->created_at }}</td>

                                        <td>

                                            <a href="{{ route('invoice.details', $invoice->id) }}" type="button" class="btn btn-primary edit">
                                                Details
                                            </a>

                                            <a href="{{ route('invoice.generatePdf', $invoice->id) }}" type="button" class="btn btn-primary edit">
                                                PDF
                                            </a>
                                        </td>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>

@endsection
@section('extra-js')
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
            //     .format('YYYY-MM-DD'));
        });
    });
</script>

<script>
    $(document).on('click', '#filter', function(e) {

        var filter_transaction_type = document.getElementById("filter_transaction_type").value;
        var filter_date_range = document.getElementById("filter_date_range").value;

        console.log(filter_date_range);
        $.ajax({
            url: "{{route('invoice.invoiceSearch')}}",
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                'filter_transaction_type': filter_transaction_type,
                'filter_date_range': filter_date_range
            },


            success: function(response) {

                try {
                    response = JSON.parse(response);
                    console.log(response);

                    if (response.success == false) {
                        alert(response.message);
                    } else {
                        // alert(response.message);
                    }
                } catch (error) {
                    $('#example2 tbody').remove();
                    $('#example2').append(response);
                }


            },
            error: function(xhr, textStatus, errorThrown) {

                console.log("Fail");
                console.log(textStatus);
                console.log(errorThrown);
                console.log(xhr);
                alert("Fail");
            }

        });
    });
</script>

@endsection