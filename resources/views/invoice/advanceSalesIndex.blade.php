@extends('layouts.app')


@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Advance Invoice List </h1>
            </div>
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Inv No</th>
                                        <th>Tran Type</th>
                                        <th>Sub Total</th>
                                        <th>Discount</th>
                                        <th>Total</th>
                                        <th>Due</th>
                                        <th>Paid</th>
                                        <th>Supplier / Customer Name</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($invoice_tran_list as $key => $invoice)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td id="t{{ $invoice->id }}">{{ $invoice->inv_no }}</td>
                                        <td>{{ $invoice->tran_type }}</td>
                                        <td>{{ $invoice->sub_total }}</td>
                                        <td>{{ $invoice->discount }}</td>
                                        <td>{{ $invoice->total }}</td>
                                        <td>{{ $invoice->due }}</td>
                                        <td>{{ $invoice->paid }}</td>
                                        
                                        @if ($invoice->tran_type == "sales to customer" || $invoice->tran_type == "Customer Return" )
                                        <td> <strong>{{ $invoice->customer->name }}</strong> ({{ $invoice->customer->contact_no }})</td>
                                        @else
                                        <td> <strong>{{ $invoice->supplier->name }}</strong> ({{ $invoice->supplier->contact_no }})</td>
                                        @endif

                                        <td>{{ $invoice->created_at }}</td>

                                        <td>

                                            <a href="{{ route('invoice.details', $invoice->id) }}" type="button" class="btn btn-primary edit">
                                                Details
                                            </a>
                                        </td>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>

@endsection

