@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Suppliers Date Wise Purchase</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">


                <div class="card">
                    <div class="card-body p-5">
                        <form method="POST" action="{{ route('reports.date-wise-purchase') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="date" name="search_date" id="search_date" value="{{ $today }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <input class="btn btn-primary" type="submit" onclick="search()" value="Search">
                                    </div>
                                </div>
                            </div>

                        </form>
                        <div class="table-responsive">
                            <table id="example33" class="table table-striped">
                                <thead>
                                    <tr class="thh2">
                                        <th>#</th>
                                        <th>Supplier Name</th>
                                        <th>Contact no</th>
                                        <th>TK</th>
                                    </tr>
                                </thead>
                                <tbody class="thh2">
                                    @foreach($list as $key2 => $data)

                                    <tr>
                                        <td> 1 </td>
                                        <td> {{ $data['supplier_details']->name  }} </td>
                                        <td> {{ $data['supplier_details']->contact_no  }} </td>
                                        <td> {{ $data['amount']  }} </td>
                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')

    <script>

    </script>

    @endsection