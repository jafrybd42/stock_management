@extends('layouts.app')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Balance History </h1>
                    <br>
                    <h1>Name : {{ $customer_name }}</h1>
                    <br>
                    <h1>Current Payable : {{ $current_payable_balance }}</h1>
                    <br>
                    <h1>Current Receivable : {{ $current_receivable_balance }}</h1>
                    <br>
                </div>
            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Last Payable Balance</th>
                                            <th>Last Receivable Balance</th>
                                            <th>Amount</th>
                                            <th>Transaction Type</th>
                                            <th>Reason</th>
                                            <th>Updated On</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($customer_balance_history as $key => $balance)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>

                                                <td>{{ $balance->last_payable }}</td>
                                                <td>{{ $balance->last_receivable }}</td>
                                                <td>{{ $balance->amount }}</td>
                                                <td>
                                                    @if ($balance->transaction_type == 0)
                                                        Payable
                                                    @elseif($balance->transaction_type == 1)
                                                        Receivable
                                                    @endif
                                                </td>
                                                <td>{{ $balance->reason }}</td>
                                                <td>{{ \Carbon\Carbon::parse($balance->last_update)->format('d/m/Y H:i:s') }}
                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>


    </div>

@endsection


@section('extra-js')
    <script>
        function changeModalData(investor) {
            console.log(investor);

            investor_data = JSON.parse(investor);

            document.getElementById("id").value = investor_data.id;
            document.getElementById("current_balance").value = investor_data.balance;
            //console.log(product_obj.id);

        }
    </script>
@endsection
