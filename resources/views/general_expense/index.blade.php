@extends('layouts.app')
@section('extra-css')
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>General Expense</h1>
                </div>


                <div class="col-6 d-flex flex-row-reverse">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                        + Add New
                    </button>
                </div>
            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Amount</th>
                                            {{-- <th>Months</th> --}}
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($expenses as $key => $expense)
                                            <tr>

                                                <td>{{ $key + 1 }}</td>
                                                <td id="t{{ $expense->id }}">{{ $expense->title }}</td>
                                                <td id=>{{ $expense->amount }}</td>

                                                <td>
                                                    <a href="{{ route('general-expense.edit', $expense->id) }}" type="button" class="btn btn-primary edit">Edit</a>
                                                    <a href="{{ route('general-expense.show', $expense->id) }}" type="button" class="btn btn-primary edit">Details</a>

                                            <form method="POST" action="{{ route('general-expense.destroy', $expense->id) }}" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                </button>
                                            </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- Modal -->
        {{-- ADD Modal --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
            <div class="modal-dialog" role="document">
                {{-- {{route('category.store')}} --}}
                <form method="POST" action="{{ route('general-expense.store') }}">
                    {{ csrf_field() }}
                    <div class=" modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add Expense</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Title <code>*</code></label>
                                <input type="text" name="title" class="form-control form-control-lg" />
                            </div>
                            <div class="form-group">
                                <label>Amount <code>*</code></label>
                                <input type="number" name="amount" class="form-control form-control-lg" />
                            </div>
                            <div class="form-group">
                                <label>Month <code>*</code></label>
                                <select name="months[]" id="month" class="form-control js-example" multiple="multiple">

                                    @foreach ($months as $key => $month)
                                        <option value="{{ $key }}">{{ $month }}</option>
                                    @endforeach

                                </select>
                            </div>


                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection


@section('extra-js')

    <script>
        $(document).ready(function() {
            $(".js-example").select2({
                width: 'resolve'
            })
        });
    </script>

@endsection