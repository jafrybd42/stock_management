@extends('layouts.app')

@section('extra-css')
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>General Expense</h1>
                </div>



            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body p-0">

                            <div class=" modal-content">
                                
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Title <code>*</code></label>
                                        <input type="text" name="title" id="title" value="{{ $expense_details->title }}"
                                            class="form-control form-control-lg" readonly />
                                    </div>

                                    <div class="form-group">
                                        <label>Amount <code>*</code></label>
                                        <input type="number" name="amount" id="amount"
                                            value="{{ $expense_details->amount }}" class="form-control form-control-lg"
                                            readonly />
                                    </div>

                                    <div class="form-group">
                                        <label>Month <code>*</code></label>
                                        @foreach ($expense_months as $key => $expense_month)

                                            @if ($id == $expense_month->general_expense_id)
                                                @foreach ($months as $key2 => $month)
                                                    @if ($key2 == $expense_month->month)
                                                        <input type="text" name="month" id="month"
                                                            value="{{ $month }}" class="form-control form-control-lg"
                                                            readonly />


                                                    @endif
                                                @endforeach
                                            @endif

                                        @endforeach

                                    </div>
                                </div>
                                <input type="hidden" name="id" id="id" value="{{ $expense_details->id }}"
                                    class="form-control form-control-lg" />
                                <div class="modal-footer bg-whitesmoke br">

                                   
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- Modal -->
        {{-- ADD Modal --}}

    </div>

@endsection


@section('extra-js')

    <script>
        $(document).ready(function() {
            $(".js-example").select2({
                width: 'resolve'
            })
        });
    </script>

@endsection
