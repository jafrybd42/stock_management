@extends('layouts.app')
@section('title', 'Product Head')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Partner List </h1>
            </div>


            <div class="col-6 d-flex flex-row-reverse">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                    + Add New
                </button>
            </div>
        </div>

        <div class="d-flex justify-content-end mb-4">
            <a class="btn btn-primary" href="{{ URL::to('/partners/pdf') }}">Export to PDF</a>
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile No</th>
                                        <th>Balance</th>
                                        <th>Percentis</th>
                                        <th>Adddress</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($partner_list as $key => $partner)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td id="t{{ $partner->id }}">{{ $partner->name }}</td>
                                        <td>{{ $partner->contact_no }}</td>
                                        <td>{{ $partner->balance }}</td>
                                        <td>{{ $partner->percentage }}</td>
                                        <td>{{ $partner->address }}</td>

                                        <td>

                                            <a href="{{ route('partner.edit', $partner->id) }}" type="button" class="btn btn-primary edit">Edit</a>
                                            <a class="btn btn-primary edit" onclick="changeModalData( '{{ $partner }}')" data-toggle="modal" data-target="#modal-part2">
                                                Update Money
                                            </a>
                                            <a href="{{ route('partner.balanceHistory', $partner->id) }}" type="button" class="btn btn-primary edit">
                                                Balance History
                                            </a>

                                            <form method="POST" action="{{ route('partner.destroy', $partner->id) }}" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Modal -->
    {{-- ADD Modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
        <div class="modal-dialog" role="document">
            {{-- {{route('category.store')}} --}}
            <form method="POST" action="{{ route('partner.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Partner</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name <code>*</code></label>
                            <input type="text" name="name" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Phone <code>*</code></label>
                            <input type="number" name="phone" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Percentage(%) <code>*</code></label>
                            <input type="number" name="percentile" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Address <code></code></label>
                            <input type="text" name="address" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Photo <code></code></label>
                            <input type="file" name="photo" class="form-control form-control-lg" />
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modal-part2" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            
            <form method="POST" action="{{ route('partner.moneyUpdate') }}" >
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Money</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Current Balance <code>*</code></label>
                            <input type="text" name="current_balance" id="current_balance" class="form-control form-control-lg" readonly />
                        </div>
                        <div class="form-group">
                            <label>Money
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <input type="number" min="1" id="newStock"  name="money" 
                                    class="form-control">
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="operator" name="operator" class="form-control" >
                                    <option value="add">+</option>
                                    <option value="subtract">-</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly />
                    </div>
                   
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" id="editProductHead" class="btn btn-primary">
                            Update Balance
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extra-js')
<script>
    function changeModalData(partner) {
        console.log(partner);
        
        partner_data = JSON.parse(partner);

        document.getElementById("id").value = partner_data.id;
        document.getElementById("current_balance").value = partner_data.balance;
        //console.log(product_obj.id);
        
    }
</script>
@endsection


