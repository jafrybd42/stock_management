@extends('layouts.app')
@section('title', 'Product Type')
    @push('css')


    @endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Product Type</h1>
                </div>


                <div class="col-6 d-flex flex-row-reverse">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                        + Add New
                    </button>
                </div>
            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($product_types as $key => $product_type)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td id="t{{ $product_type->id }}">{{ $product_type->title }}</td>

                                                <td>
                                                    
                                                    <a href="{{ route('product-type.edit', $product_type->id) }}"
                                                        type="button" class="btn btn-primary edit">Edit</a>
                                                    

                                                    <form method="POST"
                                                        action="{{ route('product-type.destroy', $product_type->id) }}"
                                                        style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class=" btn-sm btn-danger"
                                                            onclick="return confirm('Confirm delete?')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                        </button>
                                                    </form>
                                                </td>



                                            </tr>
                                        @endforeach



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- Modal -->
        {{-- ADD Modal --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
            <div class="modal-dialog" role="document">
                Type
                <form method="POST" action="{{ route('product-type.store') }}">
                    {{ csrf_field() }}
                    <div class=" modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add Product Type</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Title <code>*</code></label>
                                <input type="text" name="title" class="form-control form-control-lg" />
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- End -->
        {{-- EDIT MODAL --}}
        {{-- <div class="modal fade" id="editModal" tabindex="-1" role="dialog" >
            <div class="modal-dialog" role="document">
                
                <form method="POST" action="{{URL::to('/product-head')}}" id="editForm">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class=" modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Product Head</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Title <code>*</code></label>
                                <input type="text" name="title" id="title" class="form-control form-control-lg"  />
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div> --}}
    </div>

@endsection

@section('extra-js')

@endsection
