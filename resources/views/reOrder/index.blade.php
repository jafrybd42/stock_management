@extends('layouts.app')
@section('title', 'Products')
    @push('css')


    @endpush

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Stock List</h1>
                </div>
                <div class="col-6 d-flex flex-row-reverse">
                </div>
            </div>

            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Head</th>
                                            <th>Category</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Group</th>
                                            <th>Current Stock</th>
                                            <th>Buying Price</th>
                                            <th>Selling Price</th>
                                            <th>Average Price</th>
                                            <th>Last Transaction</th>
                                            <th>Reorder Level</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($stocks as $key => $stock)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td >
                                                    
                                                    {{ $stock->product_head->title }}
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_category_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_category->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_type_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_type->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_size_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_size->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_group_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_group->group_name }}
                                                    @endif
                                                </td>
                                                <td >{{ $stock->current_stock }}</td>
                                                <td >{{ $stock->buying_price }}</td>
                                                <td >{{ $stock->selling_price }}</td>
                                                <td >{{ $stock->average_price }}</td>
                                                <td >{{ \Carbon\Carbon::parse($stock->last_tran_date)->format('d/m/Y')}}</td>
                                                <td >{{ $stock->reorder_level }}</td>
                                                
                                                



                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    @endsection

@section('extra-js')

@endsection

