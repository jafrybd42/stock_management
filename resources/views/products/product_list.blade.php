@extends('layouts.app')
@section('title', 'Products')
    @push('css')


    @endpush

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Product List</h1>
                </div>
                <div class="col-6 d-flex flex-row-reverse">
                </div>
            </div>

            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Head</th>
                                            <th>Category</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Group</th>
                                            <th>Qty</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($product_list as $key => $product)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td >
                                                    {{ $product->product_head->title }}
                                                </td>
                                                <td >
                                                    @if ($product->product_category_id == 0)
                                                    None
                                                    @else
                                                    {{ $product->product_category->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($product->product_type_id == 0)
                                                    None
                                                    @else
                                                    {{ $product->product_type->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($product->product_size_id == 0)
                                                    None
                                                    @else
                                                    {{ $product->product_size->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($product->product_group_id == 0)
                                                    None
                                                    @else
                                                    {{ $product->product_group->group_name }}
                                                    @endif
                                                </td>
                                                
                                                <td >{{ $product->quantity }}</td>

                                                <td>

                                                    <a href="{{ route('product.edit', $product->id) }}"
                                                        type="button" class="btn btn-primary edit">Edit</a>


                                                    <form method="POST"
                                                        action="{{ route('product.destroy', $product->id) }}"
                                                        style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class=" btn-sm btn-danger"
                                                            onclick="return confirm('Confirm delete?')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                        </button>
                                                    </form>
                                                </td>



                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    @endsection

    @section('extra-js')

        <script type="text/javascript">
            $(document).ready(function() {

                function showmodal(myvalue) {
                    console.log(myvalue);

                    $('#title').val($('#t' + id).html());
                    console.log("hi");

                    var x = $('#myform').attr('action', '{{ URL::to('/product-head') }}/' + id);


                    $('#myModal2').modal({
                        backdrop: 'static'
                    });
                }

            });
        </script>
        @endsection
