@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Product Edit</h1>
            </div>

        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">

                    <div class="card-body">
                        <form method="POST" action="{{ route('product.update', $id) }}" id="editForm">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label>Select Head</label>
                                <select name="product_head" class="form-control">


                                    @foreach ($product_heads as $product_head)
                                    <option value="{{ $product_head->id }}" <?php if ($product_head->id == $product->product_head_id) {
                                                                                echo 'selected';
                                                                            } ?>>{{ $product_head->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Category</label>
                                <select name="product_category" class="form-control">
                                    @if ($product->product_category_id == 0)

                                    <option value="0" selected="selected">Select Product Categories</option>
                                    @foreach ($product_categories as $product_cat)
                                    <option value="{{ $product_cat->id }}">{{ $product_cat->title }}
                                    </option>
                                    @endforeach

                                    @else

                                    <option value="0" selected="selected">Select None</option>
                                    @foreach ($product_categories as $product_category)
                                    <option value="{{ $product_category->id }}" <?php if ($product_category->id == $product->product_category_id) {
                                                                                    echo 'selected';
                                                                                } ?>>{{ $product_category->title }}</option>
                                    @endforeach

                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Type</label>
                                <select name="product_type" class="form-control">

                                    @if ($product->product_type_id == 0)

                                    <option value="0" selected="selected">Select Product Type</option>
                                    @foreach ($product_types as $product_typ)
                                    <option value="{{ $product_typ->id }}">{{ $product_typ->title }}
                                    </option>
                                    @endforeach

                                    @else

                                    <option value="0" selected="selected">Select None</option>
                                    @foreach ($product_types as $product_type)
                                    <option value="{{ $product_type->id }}" <?php if ($product_type->id == $product->product_type_id) {
                                                                                echo 'selected';
                                                                            } ?>>{{ $product_type->title }}</option>
                                    @endforeach

                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Size</label>
                                <select name="product_size" class="form-control">
                                    @if ($product->product_size_id == 0)

                                    <option value="0" selected="selected">Select Product Size</option>
                                    @foreach ($product_sizes as $size)
                                    <option value="{{ $size->id }}">{{ $size->title }}</option>
                                    @endforeach

                                    @else
                                    <option value="0" selected="selected">Select None</option>
                                    @foreach ($product_sizes as $product_size)
                                    <option value="{{ $product_size->id }}" <?php if ($product_size->id == $product->product_size_id) {
                                                                                echo 'selected';
                                                                            } ?>>{{ $product_size->title }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select Group</label>
                                <select name="product_group" class="form-control">
                                    @if ($product->product_group_id == 0)

                                    <option value="0" selected="selected">Select Product Group</option>
                                    @foreach ($product_groups as $group)
                                    <option value="{{ $group->id }}">{{ $group->group_name }}</option>
                                    @endforeach
                                    @else
                                    <option value="0" selected="selected">Select None</option>
                                    @foreach ($product_groups as $product_group)
                                    <option value="{{ $product_group->id }}" <?php if ($product_group->id == $product->product_group_id) {
                                                                                    echo 'selected';
                                                                                } ?>>{{ $product_group->group_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <select name="quantity" id="quantity" class="form-control" onchange="checkQtyChange()">

                                    <option value="{{ $product->quantity }}" selected="selected">
                                        {{ $product->quantity }}
                                    </option>
                                    @if ($product->quantity == 'KG/TON')
                                    <option value="Box/Pcs">Box/Pcs</option>
                                    @else
                                    <option value="KG/TON">KG/TON</option>
                                    @endif
                                </select>
                            </div>



                            <div id="hidden_div" style="display: none;">Hello hidden content</div>

                            <div class="form-group" 
                                @if ($product->quantity == 'KG/TON')
                                style="display: none;"

                                @endif

                            id="number_of_quantity_div">

                                <label>Number of Quantity</label>
                                <input name="number_of_quantity" type="number" value="{{ $product->quatity_value }}" class="form-control">
                            </div>



                            <input type="hidden" name="id" id="id" value="{{ $product->id }}" <div class="card-footer text-right">
                            <button class="btn btn-primary mr-1" type="submit">Submit</button>

                    </div>

                </div>
                </form>
            </div>
        </div>
</div>
</section>


<!-- End -->
</div>



@endsection

@section('extra-js')

<script type="text/javascript">
    function checkQtyChange() {

        let value = document.getElementById('quantity').value;
        if (value == "Box/Pcs") {
            document.getElementById('number_of_quantity_div').style.display = "block";
        } else {
            document.getElementById('number_of_quantity_div').style.display = "none";
        }
    }
</script>

@endsection