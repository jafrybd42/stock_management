<tbody>
    @foreach ($product_list as $key => $product)
    <tr>
        <td>{{ $key + 1 }}</td>
        <td>
            {{ $product->product_head->title }}
        </td>
        <td>
            @if ($product->product_category_id == 0)
            None
            @else
            {{ $product->product_category->title }}
            @endif
        </td>
        <td>
            @if ($product->product_type_id == 0)
            None
            @else
            {{ $product->product_type->title }}
            @endif
        </td>
        <td>
            @if ($product->product_size_id == 0)
            None
            @else
            {{ $product->product_size->title }}
            @endif
        </td>
        <td>
            @if ($product->product_group_id == 0)
            None
            @else
            {{ $product->product_group->group_name }}
            @endif
        </td>

        <td>{{ $product->quantity }}</td>

        <td>
            {{ $product->stock }}
        </td>

        <td>
            @if ($tran_button_name == '+ Add to Receive Cart' || $tran_button_name == '+ Add to Return Cart')
            {{ $product->buying_price }} TK
            @else
            {{ $product->selling_price }} TK
            @endif
        </td>

        <td>
            <button class="btn btn-primary" onclick="changeModalDataByID('{{$product->id}}')" data-toggle="modal" data-target="#modal-part">
                {{ $tran_button_name }}
            </button>
        </td>

    </tr>
    @endforeach