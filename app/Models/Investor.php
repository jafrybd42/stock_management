<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    use HasFactory;

    public function investorsBalanceHistory()
    {
    	return $this->hasMany(InvestorsBalanceHistory::class);
    }

    public function banks()
    {
    	return $this->hasMany(Banks::class);
    }
}
