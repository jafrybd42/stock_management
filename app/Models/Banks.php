<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    use HasFactory;

    public function customer()
    {
    	return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function supplier()
    {
    	return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function partner()
    {
    	return $this->belongsTo(Partner::class, 'partner_id');
    }

    public function investor()
    {
    	return $this->belongsTo(Investor::class, 'investor_id');
    }
}
