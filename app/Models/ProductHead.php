<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductHead extends Model
{
    use HasFactory;
    protected $fillable = ['title','store_id','status','created_at','updated_at'];

    public function product()
    {
    	return $this->hasMany(Products::class);
    }
}
