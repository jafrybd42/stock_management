<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartnersBalanceHistory extends Model
{
    use HasFactory;

    public function partner()
    {
    	return $this->belongsTo(Partner::class, 'partner_id');
    }
}
