<?php

namespace App\Http\Controllers;

use App\Models\Journal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JournalController extends Controller
{

    private $common_class_obj;
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function journal()
    {
        $data = $this->common_class_obj->getJournals();

        $mainJournalList =  $data['journal'];
        $main_cr =  $data['main_cr'];
        $main_dr =  $data['main_dr'];

        return view('accounts.journal', compact('mainJournalList', 'main_cr', 'main_dr'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function trial_balance()
    {
        $journalData = $this->common_class_obj->getJournals();
        $trialBalanceEntityData = $this->common_class_obj->getTrialBalanceEntity();

        // dump($journalData);

        $mainJournalList =  $journalData['journal'];


        $trial_balance_result = array();

        for ($i = 0; $i < count($trialBalanceEntityData); $i++) {
            for ($j = 0; $j < count($mainJournalList); $j++) {
                if ($trialBalanceEntityData[$i]['name'] == $mainJournalList[$j]['name']) {
                    $trialBalanceEntityData[$i]['amount'] = $mainJournalList[$j]['final_amount'];
                    break;
                }
            }
        }

        $main_dr = 0;
        $main_cr = 0;

        for ($i = 0; $i < count($trialBalanceEntityData); $i++) {

            if ($trialBalanceEntityData[$i]['type'] == "Dr") {
                $main_dr += $trialBalanceEntityData[$i]['amount'];
            } else {
                $main_cr += $trialBalanceEntityData[$i]['amount'];
            }
        }

        return view('accounts.trial_balance', compact('trialBalanceEntityData', 'main_cr', 'main_dr'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function income_statement()
    {
        $incomeStatement = $this->common_class_obj->getIncomeStatement();
        $incomeStatementEntityData = $incomeStatement['IncomeStatementEntityData'];
        return view('accounts.income_statement', compact('incomeStatementEntityData'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function owner_equity()
    {
        $ownerEquity = $this->common_class_obj->getOwnerEquity();
        $netProfite = $ownerEquity['netProfite'];
        $capital = $ownerEquity['capital'];
        $withdraw = $ownerEquity['withdraw'];
        return view('accounts.owner_equity', compact('netProfite', 'capital', 'withdraw'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function balance_sheet()
    {

        $balanceSheetData = $this->common_class_obj->getBalanceSheet();

        $total_assets = $balanceSheetData['Total Assets'];
        $total_fixed_assets = $balanceSheetData['Total Fixed Assets'];
        $total_liability = $balanceSheetData['Total Liability'];
        $balanceSheetProperty = $balanceSheetData['IncomeStatementEntityData'];


        // dump($balanceSheet);

        // dd('balance_sheet');

        return view('accounts.balance_sheet', compact('balanceSheetProperty', 'total_assets', 'total_fixed_assets', 'total_liability'));
    }

    /**
     * Display the specified resource.
     * @return \Illuminate\Http\Response
     */
    public function cash_flow(Request $request)
    {

        $toMonthFirstDay = date('Y-m-d');
        $nextMonthFirstDate =  date('Y-m-d');
        

        if ($request->datepicker == null) {
            $todaysMonthYear = date("F, Y");
        } else {
            $todaysMonthYear = $request->datepicker;
        }

        try {
            $tempTodaysMonthYear = join("",explode(" ",$todaysMonthYear)); // Remove space  ->  "December, 2020" to "December,2020"
            $tempTodaysMonthYear = explode(",",$tempTodaysMonthYear);
 
            $month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]; 
            $temp_month = array_search($tempTodaysMonthYear[0],$month,true) + 1; // 12
 
            // Generate  toMonthFirstDay
            $temp_month = $temp_month < 10 ? ('0'.$temp_month) : $temp_month;
            $toMonthFirstDay = $tempTodaysMonthYear[1]. "-". $temp_month ."-". "01"; // 2020-12-01
 
 
            // Generate  nextMonthFirstDate
            $temp_month = $temp_month +1 ;
            if($temp_month > 12){
                $temp_month = 1;
                $tempTodaysMonthYear[1] = (int) $tempTodaysMonthYear[1] + 1;
            }
 
            $temp_month = $temp_month < 10 ? ('0'.$temp_month) : $temp_month;
            $nextMonthFirstDate = $tempTodaysMonthYear[1]. "-". $temp_month ."-". "01";  // 2021-01-01
           
         } catch (\Throwable $th) {
             //throw $th;
         }

        //  DB::enableQueryLog();

        $list = Journal::where([['status', 1], ["name", '=', 'Cash'] , ["created_at", ">=", $toMonthFirstDay],  ["created_at", "<=", $nextMonthFirstDate]])
        ->get();

        // dd(DB::getQueryLog());

        // dd($journal_list);

        return view('reports.cash_flow', compact('list', 'todaysMonthYear'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function show(Journal $journal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function edit(Journal $journal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journal $journal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journal $journal)
    {
        //
    }
}
