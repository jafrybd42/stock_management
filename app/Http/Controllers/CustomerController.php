<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Http\Controllers\CommonController;
use App\Models\CustomerBalanceHistory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{

    private $common_class_obj;
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer_list = Customer::where('status', 1)->get();
        return view('customer.index', compact('customer_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11'
        ]);

        if (Customer::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first()) {
            return redirect()->route('customer.index')->with('error', 'Customer Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->contact_no = $request->phone;
        $customer->description =  empty($request->discription) ? "" : $request->discription;
        $customer->address =  empty($request->address) ? "" : $request->address;
        $customer->created_by = Auth::user()->id;
        $customer->updated_by = Auth::user()->id;

        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/customers'), $imageName);
            $customer->image = $imageName;
        }

        $customer->save();

        if ($customer) {
            return redirect()->route('customer.index')->with('success', 'Customer ' . $request->input('name') . ' (' . $request->input('phone') . ') Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer_info = Customer::findorFail($id);
        return view('customer.edit', compact('customer_info', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11'
        ]);

        $custom_info = Customer::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('customer.index')->with('error', 'Customer Unknown');
        }

        $exitCustomerDetails = Customer::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first();

        if (!empty($exitCustomerDetails) && $exitCustomerDetails->id  != $custom_info->id) {
            return redirect()->route('customer.index')->with('error', 'Customer Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $custom_info->name = $request->name;
        $custom_info->contact_no = $request->phone;
        $custom_info->description =  empty($request->discription) ? "" : $request->discription;
        $custom_info->address =  empty($request->address) ? "" : $request->address;
        $custom_info->created_by = Auth::user()->id;
        $custom_info->updated_by = Auth::user()->id;


        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/customers'), $imageName);
            $custom_info->image = $imageName;
        }


        $custom_info->save();

        if ($custom_info) {
            return redirect()->route('customer.index')->with('success', 'Customer info Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $custom_info = Customer::findorFail($id);
        $custom_info->status = 0;
        $custom_info->save();

        return redirect()->route('customer.index')->with('success', 'Customer ' . $custom_info->name . ' ( ' . $custom_info->contact_no . ' ) Deleted');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getCustomerInfoByPhoneNo(Request $request)
    {
        $phone = $request->phoneNo;
        $customs_list =  Customer::where([['status', 1], ['contact_no', '=', $phone]])->get();

        $response =array("success" => true, "isFindData" => false , "data" => "{}", "message" => "");

        if(count($customs_list) > 0) {
            $response['isFindData'] =  true;
            $response['data'] = $customs_list[0];
        } 

        return new JsonResponse(json_encode($response));
    }

    public function payment(Request $request)
    {
        $validatedata = $request->validate([
            'amount' => 'required|min:1|numeric'
            
        ]);

        $id = $request->id;
        $customer_name = $request->customer_name;
        $amount = $request->amount;
        $transaction_type = $request->transaction_type;

        

        $customer_info = Customer::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('customer.index')->with('error', 'Customer Unknown');
        }

        $last_payable = $customer_info->payable;
        $last_receivable = $customer_info->receivable;

        /// Payable
        if($transaction_type == 0) {
            if($customer_info->payable <=0 ){
                return redirect()->route('customer.index')->with('error', 'Nothing for Payable');
            }
            else{
                if($amount >= $customer_info->payable){
                    $new_amount = $amount - $customer_info->payable;
                    if($new_amount >= 0){
                        $customer_info->payable = 0;
                        $customer_info->receivable += $new_amount; 
                    }
                    
                } else{
                    $customer_info->payable -= $amount;
                }
                
            }
        }
        /// Receivable
        else{
            if($customer_info->receivable <=0 ){
                return redirect()->route('customer.index')->with('error', 'Nothing for Receivable');
            }
            else{
                if($amount >= $customer_info->receivable){
                    $new_amount = $amount - $customer_info->receivable;
                    if($new_amount >= 0){
                        $customer_info->receivable = 0;
                        $customer_info->payable += $new_amount; 
                    }
                    
                } else{
                    $customer_info->receivable -= $amount;
                }
                
            }
        }



        $customer_info->save();

        $customerHistory = new CustomerBalanceHistory();
        $customerHistory->customer_id = $customer_info->id;
        $customerHistory->last_payable = $last_payable;
        $customerHistory->current_payable = $customer_info->payable;
        $customerHistory->last_receivable = $last_receivable;
        $customerHistory->current_receivable =  $customer_info->receivable;
        $customerHistory->amount =  $amount;
        $customerHistory->transaction_type =  $transaction_type;
        $customerHistory->reason =  "Manual";
        $customerHistory->last_update =Carbon::now()->toDateTimeString();
        $customerHistory->updated_by = Auth::user()->id;
        $customerHistory->save();

        if ($customer_info) {

            $journals = array();
            if ($transaction_type == "0") {

                array_push($journals, array(
                    "name" => "Account Receivable",
                    "type" => "Dr",
                    "tran_type" => "CustomerBalanceHistory",
                    "tran_id" => $customerHistory->id,
                    "amount" => $amount
                ));

                array_push($journals, array(
                    "name" => "Cash",
                    "type" => "Cr",
                    "tran_type" => "CustomerBalanceHistory",
                    "tran_id" => $customerHistory->id,
                    "amount" =>  $amount
                ));
            } else {

                array_push($journals, array(
                    "name" => "Cash",
                    "type" => "Dr",
                    "tran_type" => "CustomerBalanceHistory",
                    "tran_id" => $customerHistory->id,
                    "amount" => $amount
                ));

                array_push($journals, array(
                    "name" => "Account Receivable",
                    "type" => "Cr",
                    "tran_type" => "CustomerBalanceHistory",
                    "tran_id" => $customerHistory->id,
                    "amount" =>  $amount
                ));
            }

            $this->common_class_obj->saveJournal($journals);
            return redirect()->route('customer.index')->with('success', 'Customer Payment has been updated');
        }
    }

    public function balanceHistory($id)
    {
       
        
        $customer_balance_history = CustomerBalanceHistory::where('customer_id', '=', $id)->orderBy('id', 'DESC')->get();

        if(count($customer_balance_history)<1){
            return redirect()->route('customer.index')->with('error', 'No Data Exists');
        }

        $customer_name = $customer_balance_history[0]->customer->name;
        $current_payable_balance = $customer_balance_history[0]->customer->payable;
        $current_receivable_balance = $customer_balance_history[0]->customer->receivable;
        
        
        return view('customer.customerBalanceHistory', compact('customer_balance_history', 'id','customer_name','current_payable_balance','current_receivable_balance'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dueList()
    {
        $customer_due_list = Customer::where([['receivable', '>', 0], ['status', '=', 1]])->get();
        return view('customer.dueList', compact('customer_due_list'));
    }
}
