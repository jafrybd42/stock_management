<?php

namespace App\Http\Controllers;

use App\Models\Stocks;
use App\Models\ProductHead;
use App\Models\ProductGroup;
use App\Models\ProductCategory;
use App\Models\ProductSize;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;

class StocksController extends Controller
{
    private $common_class_obj;
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $stocks = Stocks::where('status', 1)->orderBy('id', 'ASC')->get();

        for ($i = 0; $i < count($stocks); $i++) {

            // Get product Head
            if ($stocks[$i]->product->product_head_id != 0) {
                $product_head_details = ProductHead::where([['id', '=', $stocks[$i]->product->product_head_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_head_details)) {
                    $stocks[$i]['product_head'] = $product_head_details;
                }
            }



            // // Get product Category
            if ($stocks[$i]->product->product_category_id != 0) {
                $product_cat_details = ProductCategory::where([['id', '=', $stocks[$i]->product->product_category_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_cat_details)) {
                    $stocks[$i]['product_category'] = $product_cat_details;
                }
            }

            // // Get product type
            if ($stocks[$i]->product->product_type_id  != 0) {
                $product_type_details = ProductType::where([['id', '=', $stocks[$i]->product->product_type_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_type_details)) {
                    $stocks[$i]['product_type'] = $product_type_details;
                }
            }


            // // Get product Size
            if ($stocks[$i]->product->product_size_id != 0) {
                $product_size_details = ProductSize::where([['id', '=', $stocks[$i]->product->product_size_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_size_details)) {
                    $stocks[$i]['product_size'] = $product_size_details;
                }
            }


            // // Get product Group
            if ($stocks[$i]->product->product_group_id != 0) {
                $product_group_details = ProductGroup::where([['id', '=', $stocks[$i]->product->product_group_id], ['status', '=', 1]])->select('group_name', 'status')->first();
                if (!empty($product_group_details)) {
                    $stocks[$i]['product_group'] = $product_group_details;
                }
            }
        }



        return view('stock.index', compact('stocks'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reOrderList()
    {
        //
        $stocks = Stocks::where('status', 1)->orderBy('id', 'ASC')->get();

        $delete_product_list = array();

        for ($i = 0; $i < count($stocks); $i++) {

            $current_stock = $stocks[$i]->current_stock;
            $reorder_level = $stocks[$i]->reorder_level;


            if ($current_stock <= $reorder_level) {

                if ($stocks[$i]->product->product_head_id != 0) {
                    $product_head_details = ProductHead::where([['id', '=', $stocks[$i]->product->product_head_id], ['status', '=', 1]])->select('title', 'status')->first();
                    if (!empty($product_head_details)) {
                        $stocks[$i]['product_head'] = $product_head_details;
                    }
                }



                // // Get product Category
                if ($stocks[$i]->product->product_category_id != 0) {
                    $product_cat_details = ProductCategory::where([['id', '=', $stocks[$i]->product->product_category_id], ['status', '=', 1]])->select('title', 'status')->first();
                    if (!empty($product_cat_details)) {
                        $stocks[$i]['product_category'] = $product_cat_details;
                    }
                }

                // // Get product type
                if ($stocks[$i]->product->product_type_id  != 0) {
                    $product_type_details = ProductType::where([['id', '=', $stocks[$i]->product->product_type_id], ['status', '=', 1]])->select('title', 'status')->first();
                    if (!empty($product_type_details)) {
                        $stocks[$i]['product_type'] = $product_type_details;
                    }
                }


                // // Get product Size
                if ($stocks[$i]->product->product_size_id != 0) {
                    $product_size_details = ProductSize::where([['id', '=', $stocks[$i]->product->product_size_id], ['status', '=', 1]])->select('title', 'status')->first();
                    if (!empty($product_size_details)) {
                        $stocks[$i]['product_size'] = $product_size_details;
                    }
                }


                // // Get product Group
                if ($stocks[$i]->product->product_group_id != 0) {
                    $product_group_details = ProductGroup::where([['id', '=', $stocks[$i]->product->product_group_id], ['status', '=', 1]])->select('group_name', 'status')->first();
                    if (!empty($product_group_details)) {
                        $stocks[$i]['product_group'] = $product_group_details;
                    }
                }
            } else {
                array_push($delete_product_list, $i);
                // unset($array[$key]);
            }
        }


        $arrayLength = count($delete_product_list);


        for ($i = $arrayLength; $i > 0; $i--) {
            unset($stocks[$delete_product_list[$i - 1]]);
        }


        return view('reOrder.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stocks  $stocks
     * @return \Illuminate\Http\Response
     */
    public function show(Stocks $stocks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stocks  $stocks
     * @return \Illuminate\Http\Response
     */
    public function edit(Stocks $stocks, $id)
    {
        //
        $stock = Stocks::findorFail($id);
        return view('stock.edit', compact('stock', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stocks  $stocks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stocks $stocks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stocks  $stocks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stocks $stocks)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function price_stock_change(Request $request, $id)
    {

        $validatedata = $request->validate([
            'buying_price' => 'required|numeric|gte:0',
            'selling_price' => 'required|numeric|gte:0',
            'reorder_level' => 'required|numeric|gte:0',
            'average_price' => 'required|numeric|gte:0',
        ]);

        $stock = Stocks::findorFail($id);

        if (
            $stock->buying_price !=  $request->buying_price || $stock->selling_price !=  $request->selling_price ||
            $stock->reorder_level !=  $request->reorder_level || $stock->average_price !=  $request->average_price
        ) {

            $stock->buying_price = empty($request->buying_price) ? 0 : $request->buying_price;
            $stock->selling_price = empty($request->selling_price) ? 0 : $request->selling_price;
            $stock->reorder_level = empty($request->reorder_level) ? 0 : $request->reorder_level;
            $stock->average_price = empty($request->average_price) ? 0 : $request->average_price;

            if ($request->new_stock == null) {
                $stock->current_stock = $stock->current_stock + 0;
            } else {

                if ($request->operator == "add") {

                    $stock->current_stock = $stock->current_stock + $request->new_stock;
                    if ($stock->current_stock < 0) {
                        return redirect()->route('stock.edit', [$id])->with('error', 'Stock value should not be negative');
                    }
                    $stock->last_tran_date = Carbon::now()->toDateTimeString();
                } elseif ($request->operator == "subtract") {
                    $stock->current_stock = $stock->current_stock - $request->new_stock;
                    if ($stock->current_stock < 0) {
                        return redirect()->route('stock.edit', [$id])->with('error', 'Stock value should not be negative');
                    }
                    $stock->last_tran_date = Carbon::now()->toDateTimeString();
                }
            }


            $stock->save();

            if ($stock) {
                return redirect()->route('stock.index')->with('success', 'Stock has been updated');
            } else {
                dd("404");
            }
        } else {

            if ($request->new_stock == null) {
                $stock->current_stock = $stock->current_stock + 0;
            } else if ($request->operator == "add") {

                $stock->current_stock = $stock->current_stock + $request->new_stock;
                if ($stock->current_stock < 0) {
                    return redirect()->route('stock.edit', [$id])->with('error', 'Stock value should not be negative');
                }
                $stock->last_tran_date = Carbon::now()->toDateTimeString();
            } elseif ($request->operator == "subtract") {
                $stock->current_stock = $stock->current_stock - $request->new_stock;
                if ($stock->current_stock < 0) {
                    return redirect()->route('stock.edit', [$id])->with('error', 'Stock value should not be negative');
                }
                $stock->last_tran_date = Carbon::now()->toDateTimeString();
            }

            $stock->save();

            if ($stock) {

                if ($request->operator == "subtract") {
                    $journals = array();

                    array_push($journals, array(
                        "name" => "Damage",
                        "type" => "Dr",
                        "tran_type" => "Stock Update -",
                        "tran_id" => $stock->id,
                        "amount" => $request->new_stock * $stock->buying_price
                    ));

                    array_push($journals, array(
                        "name" => "Purchase",
                        "type" => "Cr",
                        "tran_type" => "Stock Update - ",
                        "tran_id" => $stock->id,
                        "amount" =>  $request->new_stock * $stock->buying_price
                    ));

                    $this->common_class_obj->saveJournal($journals);
                }

                return redirect()->route('stock.index')->with('success', 'Stock has been updated');
            } else {
                dd("404");
            }
        }
    }
}
