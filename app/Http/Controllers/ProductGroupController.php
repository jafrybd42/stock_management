<?php

namespace App\Http\Controllers;

use App\Models\ProductGroup;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ProductGroupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $product_groups = ProductGroup::where('status', 1)->get();
        
        return view('product_group.index', compact('product_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedata = $request->validate([
            'group_name' => 'required|max:255'
        ]);

        if (ProductGroup::where([['group_name', '=', $request->input('group_name')],['status','=',1]])->first()) {
            return redirect()->route('product-group.index')->with('error', 'Product Group Already Exists');
         }

        $product_group = new ProductGroup();
        $product_group->group_name = $request->group_name;                    
        $product_group->created_by = Auth::user()->id;                   
        $product_group->save();

        if ($product_group) {
            return redirect()->route('product-group.index')->with('success', 'Product Group Added');
        }
        // else{
        //     return redirect()->route('product-group.index')->with('error', 'Product  Group Exists');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductGroup  $productGroup
     * @return \Illuminate\Http\Response
     */
    public function show(ProductGroup $productGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductGroup  $productGroup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product_group = ProductGroup::findorFail($id);
        
        return view('product_group.edit', compact('product_group', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductGroup  $productGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $validatedata = $request->validate([
            'group_name' => 'required|max:255'
        ]);

        $product_group = ProductGroup::findorFail($id);
        if($product_group->group_name != $request->group_name){

            
             $existingData = ProductGroup::where([['group_name', '=', $request->input('group_name')], ['status', '=', 1]])->first();

            
            if (!empty($existingData) && $existingData->id  != $product_group->id) {
                return redirect()->route('product-group.index')->with('error', 'Product Group Already Exists');
            }

             $product_group->group_name = $request->group_name;
             $product_group->save();
            
            if ($product_group) {
                return redirect()->route('product-group.index')->with('success', 'Product Group Updated');
            }
        }
        else{
            return redirect()->route('product-group.index');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductGroup  $productGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product_group = ProductGroup::findorFail($id);

        try {
            $exit_product_list = Products::where([['status', '=', 1], ['product_group_id', $id]])->get();
            if (count($exit_product_list) > 0) {
                return redirect()->route('product-group.index')->with('error', 'You can\'t Delete this Product Group, It has Product.');
            }
        } catch (\Throwable $th) {
            return redirect()->route('product-group.index')->with('error', 'You can\'t Delete this Product Group, It has Product.');
        }
        
        $product_group->status =0;
        $product_group->save();

        return redirect()->route('product-group.index')->with('success', 'Product Group Delete');
    }
}
