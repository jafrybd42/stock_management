<?php

namespace App\Http\Controllers;

use App\Models\GeneralExpense;
use App\Models\GeneralExpenseMonth;
use App\Http\Controllers\CommonController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class GeneralExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $expenses = GeneralExpense::where('status', 1)->orderBy('id', 'DESC')->get();


        $months = $this->common_class_obj->getMonthsName();
        
        // $commonController = new CommonController();
        // $months = $commonController->getMonthsName();
         //dd($months);


        return view('general_expense.index', compact('expenses','months'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255|min:3',
            'amount' => 'required|min:1|numeric',
            //'month' => 'required',
        ]);
        if($request->months == null){
            return redirect()->route('general-expense.index')->with('error', 'You have to select a month');
        }
        // general expense
        
        $expense = new GeneralExpense();
        $expense->title = $request->title;
        $expense->amount = $request->amount;
        $expense->created_by = Auth::user()->id;
        $expense->updated_by = Auth::user()->id;
        $expense->save();

        /// save month data
        $months = $request->months;
        

        for ($i = 0; $i < count($months); $i++) {

            $month[$i] = new GeneralExpenseMonth();
            $month[$i]->general_expense_id = $expense->id;
            $month[$i]->month = (int)($months[$i]);
            $month[$i]->created_by = Auth::user()->id;
            $month[$i]->updated_by = Auth::user()->id;
            $month[$i]->save();


        }

        if ($expense) {
            return redirect()->route('general-expense.index')->with('success', 'Expense has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $expense_details = GeneralExpense::findorFail($id);
        if($expense_details->status != 0)
        {
            $expense_months = GeneralExpenseMonth::where('general_expense_id', $id)->get();
        }

        $months = $this->common_class_obj->getMonthsName();
        return view('general_expense.details', compact('expense_details','months','id','expense_months'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $expense_details = GeneralExpense::findorFail($id);
        if($expense_details->status != 0)
        {
            $expense_months = GeneralExpenseMonth::where('general_expense_id', $id)->get();
        }

        $months = $this->common_class_obj->getMonthsName();
        return view('general_expense.edit', compact('expense_details','months','id','expense_months'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255|min:3',
            'amount' => 'required|min:1|numeric',
            //'date' => 'required',
        ]);

        $expense = GeneralExpense::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('general-expense.index')->with('error', 'Unknown Data');
        }

        $expense->title = $request->title;
        $expense->amount = $request->amount;
        $expense->created_by = Auth::user()->id;
        $expense->updated_by = Auth::user()->id;
        $expense->save();

        /// save month data

        $expense_months = GeneralExpenseMonth::where('general_expense_id', $id)->get();
        //dd($expense_months);

        $months = $request->months;
        
        $requested_months = [];
            for ($i = 0; $i < count($request->months); $i++) { 
                    array_push($requested_months, (int)($months[$i]));
            }
        //sort($requested_months);
        

        if(count($requested_months) == count($expense_months))
        {
            for($i = 0; $i < count($requested_months); $i++){
                if($requested_months[$i] != $expense_months[$i]->month)
                {
                    $month_details = GeneralExpenseMonth::where([['general_expense_id', '=', $id], ['month', '=', $expense_months[$i]->month]])->get();
                    
                    $month_details[0]->month = $requested_months[$i];
                    
                    $month_details[0]->updated_by = Auth::user()->id;
                    
                    $month_details[0]->save();
                   
                }
            }
        } else if(count($requested_months) > count($expense_months)){

            $extraData = abs(count($requested_months)- count($expense_months));

            for($i = 0; $i < count($expense_months); $i++){
                if($requested_months[$i] != $expense_months[$i]->month)
                {
                    $month_details = GeneralExpenseMonth::where([['general_expense_id', '=', $id], ['month', '=', $expense_months[$i]->month]])->get();
                    
                    $month_details[0]->month = $requested_months[$i];
                    
                    $month_details[0]->updated_by = Auth::user()->id;
                    
                    $month_details[0]->save();
                   
                }
            }

            for($j = count($expense_months) ; $j < count($requested_months); $j++){

            $month[$j] = new GeneralExpenseMonth();
            $month[$j]->general_expense_id = $expense->id;
            $month[$j]->month = $requested_months[$j];
            $month[$j]->created_by = Auth::user()->id;
            $month[$j]->updated_by = Auth::user()->id;
            $month[$j]->save();

            }

        } else{

            $pointer = 0;
                
                for ($i = 0; $i < count($requested_months); $i++) {
                   
                    $month_details = GeneralExpenseMonth::where([['general_expense_id', '=', $id], ['month', '=', $expense_months[$i]->month]])->get();
                    
                    $month_details[0]->month = $requested_months[$i];
                    
                    $month_details[0]->updated_by = Auth::user()->id;
                    
                    $month_details[0]->save();
                    
                    $pointer = $i + 1;
            }
            

            for ($i = $pointer; $i < count($expense_months); $i++) {
                
                $month_details = GeneralExpenseMonth::where([['general_expense_id', '=', $id], ['month', '=', $expense_months[$i]->month]])->get();
                $month_details[0]->delete();
                
            }

        }
        
        if ($expense) {
            return redirect()->route('general-expense.index')->with('success', 'Expense Details Updated');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $expense = GeneralExpense::findorFail($id);

        $expense->status = 0;
        $expense->updated_by = Auth::user()->id;
        $expense->save();

        return redirect()->route('general-expense.index')->with('success', 'Expense has been Deleted');
    }
}
