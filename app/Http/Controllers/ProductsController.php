<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Stocks;
use App\Models\ProductHead;
use App\Models\ProductGroup;
use App\Models\ProductCategory;
use App\Models\ProductSize;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_list = Products::where('status', 1)->get();

        // $product_list[0]->product_head_id;
        // dump($product_list[0]['product_head_id']);


        for ($i = 0; $i < count($product_list); $i++) {

            // Get product Head
            if ($product_list[$i]['product_head_id'] != 0) {
                $product_head_details = ProductHead::where([['id', '=', $product_list[$i]['product_head_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_head_details)) {
                    $product_list[$i]['product_head'] = $product_head_details;
                }
            }

            // Get product Category
            if ($product_list[$i]['product_category_id'] != 0) {
                $product_cat_details = ProductCategory::where([['id', '=', $product_list[$i]['product_category_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_cat_details)) {
                    $product_list[$i]['product_category'] = $product_cat_details;
                }
            }

            // Get product type
            if ($product_list[$i]['product_type_id'] != 0) {
                $product_type_details = ProductType::where([['id', '=', $product_list[$i]['product_type_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_type_details)) {
                    $product_list[$i]['product_type'] = $product_type_details;
                }
            }


            // Get product Size
            if ($product_list[$i]['product_size_id'] != 0) {
                $product_size_details = ProductSize::where([['id', '=', $product_list[$i]['product_size_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_size_details)) {
                    $product_list[$i]['product_size'] = $product_size_details;
                }
            }


            // Get product Group
            if ($product_list[$i]['product_group_id'] != 0) {
                $product_group_details = ProductGroup::where([['id', '=', $product_list[$i]['product_group_id']], ['status', '=', 1]])->select('group_name', 'status')->first();
                if (!empty($product_group_details)) {
                    $product_list[$i]['product_group'] = $product_group_details;
                }
            }
        }

        // dump($product_list[0]->product_head['title']);
        // dump($product_list);
        // dd($product_list);

        // $products = DB::table('products')
        //     ->join('product_heads', 'products.product_head_id', 'product_heads.id')
        //     ->join('product_categories', 'products.product_category_id', 'product_categories.id')
        //     ->join('product_types', 'products.product_type_id', 'product_types.id')
        //     ->join('product_sizes', 'products.product_size_id', 'product_sizes.id')
        //     ->join('product_groups', 'products.product_group_id', 'product_groups.id')
        //     ->select(
        //         'products.*',
        //         'product_heads.id as product_head_id',
        //         'product_heads.title as head_title',
        //         'product_categories.id as product_cat_id',
        //         'product_categories.title as category_title',
        //         'product_types.id as product_type_id',
        //         'product_types.title as type_title',
        //         'product_sizes.id as product_size_id',
        //         'product_sizes.title as size_title',
        //         'product_groups.id as product_group_id',
        //         'product_groups.group_name'
        //     )
        //     ->where('products.status', '1')
        //     ->get();

        // // dd($products);

        return view('products.product_list', compact('product_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $product_heads = ProductHead::where('status', 1)->get();
        $product_categories = ProductCategory::where('status', 1)->get();
        $product_types = ProductType::where('status', 1)->get();
        $product_sizes = ProductSize::where('status', 1)->get();
        $product_groups = ProductGroup::where('status', 1)->get();

        return view('products.product_add', compact('product_heads', 'product_categories', 'product_types', 'product_sizes', 'product_groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedata = $request->validate([
            'product_head' => 'required|numeric',
            'product_category' => 'required|numeric',
            'product_type' => 'required|numeric',
            'product_size' => 'required|numeric',
            'product_group' => 'required|numeric',
            'quantity' => 'required'
        ]);

        if ($request->product_head == 0) {
            return redirect()->route('product.create')->with('error', 'You have to select Product Head and Price');
        }


        $request->product_category = $request->product_category == 0 ? null : $request->product_category;
        $request->product_type = $request->product_type == 0 ? null : $request->product_type;
        $request->product_size = $request->product_size == 0 ? null : $request->product_size;
        $request->product_group = $request->product_group == 0 ? null : $request->product_group;

        $existingProductInfo = Products::where([
            ['product_head_id', '=', $request->product_head],
            ['product_category_id', '=', $request->product_category],
            ['product_type_id', '=', $request->product_type],
            ['product_size_id', '=', $request->product_size],
            ['product_group_id', '=', $request->product_group],
            ['status', '=', 1],
        ])->select('id', 'status')->first();


        if (!empty($existingProductInfo)) {
            return redirect()->route('product.create')->with('error', 'Product Already Added');
        }


        $product = new Products();
        $product->product_head_id = $request->product_head;
        $product->product_category_id = $request->product_category == "0" ? null : $request->product_category;
        $product->product_type_id = $request->product_type == "0" ? null : $request->product_type;
        $product->product_size_id =  $request->product_size == "0" ? null : $request->product_size;
        $product->product_group_id =  $request->product_group == "0" ? null : $request->product_group;
        $product->quantity =  $request->quantity;
        $product->quatity_value = empty($request->number_of_quantity) ? 0 : $request->number_of_quantity;
        $product->created_by = Auth::user()->id;
        $product->save();

        $stock = new Stocks();
        $stock->product_id = $product->id;
        $stock->created_by = Auth::user()->id;
        $stock->save();

        if ($product) {
            return redirect()->route('product.index')->with('success', 'Product Successfully Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product = Products::findorFail($id);
        $product_heads = ProductHead::where('status', 1)->get();
        $product_categories = ProductCategory::where('status', 1)->get();
        $product_types = ProductType::where('status', 1)->get();
        $product_sizes = ProductSize::where('status', 1)->get();
        $product_groups = ProductGroup::where('status', 1)->get();

        return view('products.product_edit', compact('product', 'id', 'product_heads', 'product_categories', 'product_types', 'product_sizes', 'product_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedata = $request->validate([
            'product_head' => 'required'
        ]);

        $product = Products::findorFail($id);

        if ($request->product_head == 0) {
            return redirect()->route('product.edit')->with('warning', 'You have to select Product Head and Price');
        }

        if (
            $product->product_head_id !=  $request->product_head || $product->product_category_id !=  $request->product_category ||
            $product->product_type_id !=  $request->product_type || $product->product_size_id !=  $request->product_size
            || $product->product_group_id !=  $request->product_group
        ) {

            $request->product_category = $request->product_category == 0 ? null : $request->product_category;
            $request->product_type = $request->product_type == 0 ? null : $request->product_type;
            $request->product_size = $request->product_size == 0 ? null : $request->product_size;
            $request->product_group = $request->product_group == 0 ? null : $request->product_group;

            $existingProductInfo = Products::where([
                ['product_head_id', '=', $request->product_head],
                ['product_category_id', '=', $request->product_category],
                ['product_type_id', '=', $request->product_type],
                ['product_size_id', '=', $request->product_size],
                ['product_group_id', '=', $request->product_group],
                ['status', '=', 1],
            ])->select('id', 'status')->first();

            if (!empty($existingProductInfo)) {
                return redirect()->route('product.edit', [$id])->with('error', 'Product Already Exists');
            }

            $product->product_head_id = $request->product_head;
            $product->product_category_id = $request->product_category == "0" ? null : $request->product_category;
            $product->product_type_id = $request->product_type == "0" ? null : $request->product_type;
            $product->product_size_id =  $request->product_size == "0" ? null : $request->product_size;
            $product->product_group_id =  $request->product_group == "0" ? null : $request->product_group;
            $product->quantity =  $request->quantity;
            $product->quatity_value = empty($request->number_of_quantity) ? 0 : $request->number_of_quantity;
            $product->save();

            if ($product) {
                return redirect()->route('product.index')->with('success', 'Product Successfully Updated');
            }
        } else if (
            $product->product_head_id ==  $request->product_head && $product->product_category_id ==  $request->product_category &&
            $product->product_type_id ==  $request->product_type && $product->product_size_id ==  $request->product_size
            && $product->product_group_id ==  $request->product_group && $product->quantity == $request->quantity &&
            $product->quatity_value == $request->number_of_quantity
        ) {

            return redirect()->route('product.index')->with('success', 'Nothing to change');
        } else if (
            $product->product_head_id ==  $request->product_head && $product->product_category_id ==  $request->product_category &&
            $product->product_type_id ==  $request->product_type && $product->product_size_id ==  $request->product_size
            && $product->product_group_id ==  $request->product_group
        ) {

            if (
                $product->quantity != $request->quantity ||
                $product->quatity_value != $request->number_of_quantity
            ) {
                $product->quantity =  $request->quantity;
                $product->quatity_value = empty($request->number_of_quantity) ? 0 : $request->number_of_quantity;
                $product->save();
                if ($product) {
                    return redirect()->route('product.index')->with('success', 'Product Successfully Updated');
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::findorFail($id);
        $product->status = 0;

        $product->save();
        return redirect()->route('product.index')->with('success', 'Product Successfully Deleted');
    }


    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function detailsByID(Request $request)
    {

        $product_id = $request->product_id;
        $product = Products::where([
            ['id', '=', $product_id],
            ['status', '=', 1],
        ])->first();


        if (empty($product)) {
            $product =  "{}";
            $success = false;
        } else {
            $success = true;
            $stock_details = Stocks::where([['status', '=', 1], ['product_id', $product_id]])->get();

            if (!empty($stock_details)) {
                $product['stock'] = $stock_details[0]['current_stock'];
                $product['selling_price'] = $stock_details[0]['selling_price'];
                $product['buying_price'] = $stock_details[0]['buying_price'];
            } else {
                $product['stock'] = 0;
                $product['selling_price'] = 0;
                $product['buying_price'] = 0;
            }
        }



        $json = json_encode(array("success" => $success, "message" => "Product not found.", "data" => $product));
        return new JsonResponse($json);
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $product_head_id = $request->product_head_id == null ? 0 : $request->product_head_id;
        $product_size_id = $request->product_size_id == null ? 0 : $request->product_size_id;
        $product_category_id = $request->product_category_id == null ? 0 : $request->product_category_id;
        $product_type_id = $request->product_type_id == null ? 0 : $request->product_type_id;
        $product_group_id = $request->product_group_id == null ? 0 : $request->product_group_id;
        $tranType = $request->tranType == null ? "no" : $request->tranType;

        $tran_button_name = ["+ Add To List", "Add to Return Cart", "+ Add to Receive Cart", "+ Add to Return Cart"];
        // Set Butto name
        if ($tranType == 'sales') {
            $tran_button_name =  $tran_button_name[0];
        } else if ($tranType == 'customer-return') {
            $tran_button_name =  $tran_button_name[1];
        } else if ($tranType == 'supplier-receive') {
            $tran_button_name =  $tran_button_name[2];
        } else {
            $tran_button_name =  $tran_button_name[3];
        }

        // $tran_button_name = $request->tranType;

        $query = FacadesDB::table('products');
        $query->where('status', '=',  1);


        if ($product_head_id != 0) {
            $query->where('product_head_id', '=',  $product_head_id);
        }

        if ($product_size_id != 0) {
            $query->where('product_size_id', '=',  $product_size_id);
        }

        if ($product_category_id != 0) {
            $query->where('product_category_id', '=',  $product_category_id);
        }

        if ($product_type_id != 0) {
            $query->where('product_type_id', '=',  $product_type_id);
        }

        if ($product_group_id != 0) {
            $query->where('product_group_id', '=',  $product_group_id);
        }

        try {
            $product_list = $query->get();
        } catch (\Throwable $th) {
            $json = json_encode(array("success" => true, "message" => $th,));
            return new JsonResponse($json);
        }


        for ($i = 0; $i < count($product_list); $i++) {
            // json_decode($product_list[$i], true);
            $stock_details = Stocks::where([['status', '=', 1], ['product_id', $product_list[$i]->id]])->get();


            if (!empty($stock_details)) {
                $product_list[$i]->stock = $stock_details[0]['current_stock'];
                $product_list[$i]->selling_price = $stock_details[0]['selling_price'];
                $product_list[$i]->buying_price = $stock_details[0]['buying_price'];
            } else {
                $product_list[$i]->stock = 0;
                $product_list[$i]->selling_price = 0;
                $product_list[$i]->buying_price = 0;
            }



            // Get product Head
            if ($product_list[$i]->product_head_id != 0) {
                $product_head_details = ProductHead::where([['id', '=', $product_list[$i]->product_head_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_head_details)) {
                    $product_list[$i]->product_head = $product_head_details;
                }
            }


            // Get product Category
            if ($product_list[$i]->product_category_id != 0) {
                $product_cat_details = ProductCategory::where([['id', '=', $product_list[$i]->product_category_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_cat_details)) {
                    $product_list[$i]->product_category = $product_cat_details;
                }
            }

            // Get product type
            if ($product_list[$i]->product_type_id != 0) {
                $product_type_details = ProductType::where([['id', '=', $product_list[$i]->product_type_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_type_details)) {
                    $product_list[$i]->product_type = $product_type_details;
                }
            }


            // Get product Size
            if ($product_list[$i]->product_size_id != 0) {
                $product_size_details = ProductSize::where([['id', '=', $product_list[$i]->product_size_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_size_details)) {
                    $product_list[$i]->product_size = $product_size_details;
                }
            }


            // Get product Group
            if ($product_list[$i]->product_group_id != 0) {
                $product_group_details = ProductGroup::where([['id', '=', $product_list[$i]->product_group_id], ['status', '=', 1]])->select('group_name', 'status')->first();
                if (!empty($product_group_details)) {
                    $product_list[$i]->product_group = $product_group_details;
                }
            }
        }


        json_encode($product_list);
        return view('products.product_table_body', compact('product_list', 'tran_button_name'));
    }
}
