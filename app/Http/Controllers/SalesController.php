<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\ProductHead;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\ProductType;
use App\Models\Sales;
use App\Models\Stocks;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isNull;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_list = Products::where('status', 1)->get();

        // $product_list[0]->product_head_id;
        // dump($product_list[0]['product_head_id']);



        for ($i = 0; $i < count($product_list); $i++) {


            $stock_details = Stocks::where([['status', '=', 1], ['product_id', $product_list[$i]->id]])->get();
            $product_head_list = ProductHead::where('status', '=', 1)->get();
            $product_size_list = ProductSize::where('status', '=', 1)->get();
            $product_cat_list = ProductCategory::where('status', '=', 1)->get();
            $product_type_list = ProductType::where('status', '=', 1)->get();
            $product_group_list = ProductGroup::where('status', '=', 1)->get();


            //dd($stock_details);

            if (!empty($stock_details)) {
                $product_list[$i]['stock'] = $stock_details[0]['current_stock'];
                $product_list[$i]['selling_price'] = $stock_details[0]['selling_price'];
                $product_list[$i]['buying_price'] = $stock_details[0]['buying_price'];
            } else {
                $product_list[$i]['stock'] = 0;
                $product_list[$i]['selling_price'] = 0;
                $product_list[$i]['buying_price'] = 0;
            }



            // Get product Head
            if ($product_list[$i]['product_head_id'] != 0) {
                $product_head_details = ProductHead::where([['id', '=', $product_list[$i]['product_head_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_head_details)) {
                    $product_list[$i]['product_head'] = $product_head_details;
                }
            }

            // Get product Category
            if ($product_list[$i]['product_category_id'] != 0) {
                $product_cat_details = ProductCategory::where([['id', '=', $product_list[$i]['product_category_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_cat_details)) {
                    $product_list[$i]['product_category'] = $product_cat_details;
                }
            }

            // Get product type
            if ($product_list[$i]['product_type_id'] != 0) {
                $product_type_details = ProductType::where([['id', '=', $product_list[$i]['product_type_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_type_details)) {
                    $product_list[$i]['product_type'] = $product_type_details;
                }
            }


            // Get product Size
            if ($product_list[$i]['product_size_id'] != 0) {
                $product_size_details = ProductSize::where([['id', '=', $product_list[$i]['product_size_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_size_details)) {
                    $product_list[$i]['product_size'] = $product_size_details;
                }
            }


            // Get product Group
            if ($product_list[$i]['product_group_id'] != 0) {
                $product_group_details = ProductGroup::where([['id', '=', $product_list[$i]['product_group_id']], ['status', '=', 1]])->select('group_name', 'status')->first();
                if (!empty($product_group_details)) {
                    $product_list[$i]['product_group'] = $product_group_details;
                }
            }
        }

        // dd($product_list);


        return view('sales.index', compact('product_list', 'product_head_list', 'product_size_list', 'product_cat_list', 'product_type_list',  'product_group_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function show(Sales $sales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function edit(Sales $sales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sales $sales)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sales $sales)
    {
        //
    }


    /**
     * 
     *
     * @param  \App\Models\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function add_to_cart(Request $request)
    {
        $product_id = $request->product_id;
        $qty = $request->qty;
        $qty_as = $request->qty_as;
        $sales_type = $request->sales_type == null ? 0 : $request->sales_type ;
        $transaction_type = 'Sales';

        // Check Product id & qty
        try {
            $product_id = (int)$product_id;
            $qty = (float)$qty;
        } catch (\Throwable $th) {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Request Data Formate Error")));
        }

        // Get product details & Stock Details
        $product_details = Products::where([['status', '=', 1], ['id', $request->product_id]])->get();
        $stock_details = Stocks::where([['status', '=', 1], ['product_id', $request->product_id]])->get();

        if (count($product_details) > 0) {
            $product_details = $product_details[0];
        } else {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product")));
        }

        // Check qty calculation method & calculation qty
        // 1 ton = 907.185 kg 
        $temp_qty = 0;

        if (strtoupper($product_details->quantity) == "KG/TON") {
            if (strtoupper($qty_as) == "KG") {
                $temp_qty = $qty;
            } else if (strtoupper($qty_as) == "TON") {
                $temp_qty = $qty * 907.185;
            } else {
                return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product Qty Method", "qty_as" => $qty_as)));
            }
        } else {
            if (strtoupper($qty_as) == "BOX") {
                try {
                    $temp_qty =  $qty * $product_details->quatity_value;
                } catch (\Throwable $th) {
                    return new JsonResponse(json_encode(array("success" => false, "message" => "Try catch error", "qty_as" => $product_details)));
                }
            } else if (strtoupper($qty_as) == "PCS") {
                $temp_qty = $qty;
            } else {
                return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product Qty Method", "qty_as" => $qty_as)));
            }
        }

        if (count($stock_details) > 0) {
            $stock_details = $stock_details[0];
        } else {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product Stock")));
        }

        // Check this product is already added in Carts
        $carts_list = Carts::where([['created_by', Auth::user()->id], ['product_id', '=', $product_id],['transaction_type', '=',$transaction_type]])->orderBy('id', 'ASC')->get();
        $qty_already_added = 0;

        // calculate qty which already added
        for ($i = 0; $i < count($carts_list); $i++) {
            if (strtoupper($carts_list[$i]->quantity_as) == "BOX") {
                $qty_already_added = $carts_list[$i]->quantity * $product_details->quatity_value;
            } else if (strtoupper($carts_list[$i]->quantity_as) == 'TON') {
                $qty_already_added = $carts_list[$i]->quantity * 907.185;
            } else {
                $qty_already_added = $carts_list[$i]->quantity;
            }
            break;
        }
        

        if (($temp_qty + $qty_already_added) > $stock_details->current_stock) {
            if($sales_type != 1){ // if it's 1 then we allow advance sales
                return new JsonResponse(json_encode(array("success" => false, "message" => "Stock not available")));
            }
        } else if ($qty < 1) {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Please give valid qty which is getter then zero")));
        }

        if (count($carts_list) > 0) {
            $cart = $carts_list[0];
            
            if(strtoupper($qty_as) != strtoupper($carts_list[0]->quantity_as)){
                $cart->quantity = $temp_qty + $qty_already_added; 

                if(strtoupper($qty_as) == "BOX" || strtoupper($qty_as) == "PCS"){
                    $cart->quantity_as = strtoupper("PCS");
                } else {
                    $cart->quantity_as = strtoupper("KG");
                }
            } else {
                $cart->quantity = $cart->quantity + $qty;
            }

            $cart->is_allow_advance_sale =  $sales_type == 1 ? 1 : 0;

        } else {
            $cart = new Carts();
            $cart->product_id = $product_id;
            $cart->quantity_as = strtoupper($qty_as);
            $cart->quantity = $qty;
            $cart->selling_price = $stock_details->selling_price;
            $cart->transaction_type = $transaction_type;
            $cart->is_allow_advance_sale =  $sales_type == 1 ? 1 : 0;
            $cart->created_by =  Auth::user()->id;
        }

        $cart->save();

        $json = json_encode(array("success" => true, "message" => "Product added to cart.", "carts_list" => $carts_list));
        return new JsonResponse($json);
    }
}
