<?php

namespace App\Http\Controllers;

use App\Models\Journal;
use App\Models\Carts;
use App\Models\Customer;
use App\Models\GeneralExpense;
use App\Models\GeneralExpenseMonth;
use App\Models\InvoiceTran;
use App\Models\InvoiceTranItem;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\ProductHead;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\ProductType;
use App\Models\Sales;
use App\Models\Stocks;
use App\Models\Supplier;
use App\Models\InvoiceItem;
use \PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommonController extends Controller
{

  public function saveJournal($journals)
  {

    try {
      for ($i = 0; $i < count($journals); $i++) {

        $existJournal = Journal::where([
          ['status', 1],
          ['name', "=", $journals[$i]['name']],
          ['type', "=", $journals[$i]['type']],
          ['tran_type', "=", $journals[$i]['tran_type']],
          ['tran_id', "=", $journals[$i]['tran_id']],
        ])->get();

        if (count($existJournal) > 0) {  //update existing Journal
          $journal = $existJournal[0];

          if (count($existJournal) > 1) {
            // delete extra journal 
            for ($j = 1; $j < count($existJournal); $j++) {
              $existJournal[$j]->delete();
            }
          }
        } else {
          //Create new Journal
          $journal = new Journal();
        }

        try {
          if ($journals[$i]['description'] != null) {
            $journal->description = $journals[$i]['description'];
          }
        } catch (\Throwable $th) {
          //throw $th;
        }

        $journal->name = $journals[$i]['name'];
        $journal->type = $journals[$i]['type'];
        $journal->tran_type = $journals[$i]['tran_type'];
        $journal->tran_id = $journals[$i]['tran_id'];
        $journal->amount = $journals[$i]['amount'];
        $journal->created_by = Auth::user()->id;
        $journal->updated_by = Auth::user()->id;
        $journal->save();
      }
    } catch (\Throwable $th) {
      return false;
    }


    return true;
  }

  public function generateJournalFromInvoice(\App\Models\InvoiceTran $invoiceTran)
  {
    $journals = array();

    if ($invoiceTran->tran_type == "sales to customer") {

      // ------ Cr ------
      array_push($journals, array(
        "name" => "Sales",
        "type" => "Cr",
        "tran_type" => "invoice",
        "tran_id" => $invoiceTran->id,
        "amount" => $invoiceTran->total,
      ));

      // ------- Dr -------

      if ($invoiceTran->paid > 0) {
        array_push($journals, array(
          "name" => "Cash",
          "type" => "Dr",
          "tran_type" => "invoice",
          "description" => "Product Sales",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->paid,
        ));
      }


      if ($invoiceTran->due > 0) {
        array_push($journals, array(
          "name" => "Account Receivable",
          "type" => "Dr",
          "tran_type" => "invoice",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->due,
        ));
      }
    } else if ($invoiceTran->tran_type == "Customer Return") {

      // ------ Dr ------
      array_push($journals, array(
        "name" => "Sales Return",
        "type" => "Dr",
        "tran_type" => "invoice",
        "tran_id" => $invoiceTran->id,
        "amount" => $invoiceTran->total,
      ));

      // ------- Cr -------

      if ($invoiceTran->paid > 0) {
        array_push($journals, array(
          "name" => "Cash",
          "type" => "Cr",
          "tran_type" => "invoice",
          "description" => "Product Return",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->paid,
        ));
      }


      if ($invoiceTran->due > 0) {
        array_push($journals, array(
          "name" => "Account Receivable",
          "type" => "Cr",
          "tran_type" => "invoice",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->due,
        ));
      }
    } else if ($invoiceTran->tran_type == "Supplier Receive") {

      // ------ Dr ------
      array_push($journals, array(
        "name" => "Purchase",
        "type" => "Dr",
        "tran_type" => "invoice",
        "tran_id" => $invoiceTran->id,
        "amount" => $invoiceTran->total,
      ));

      // ------- Cr -------

      if ($invoiceTran->paid > 0) {
        array_push($journals, array(
          "name" => "Cash",
          "type" => "Cr",
          "tran_type" => "invoice",
          "description" => "Product purchase (supplier)",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->paid,
        ));
      }


      if ($invoiceTran->due > 0) {
        array_push($journals, array(
          "name" => "Account Payable",
          "type" => "Cr",
          "tran_type" => "invoice",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->due,
        ));
      }
    } else if ($invoiceTran->tran_type == "Supplier Return") {

      // ------ Cr ------
      array_push($journals, array(
        "name" => "Purchase Return",
        "type" => "Cr",
        "tran_type" => "invoice",
        "tran_id" => $invoiceTran->id,
        "amount" => $invoiceTran->total,
      ));

      // ------- Dr -------

      if ($invoiceTran->paid > 0) {
        array_push($journals, array(
          "name" => "Cash",
          "type" => "Dr",
          "tran_type" => "invoice",
          "description" => "Product return (supplier)",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->paid,
        ));
      }


      if ($invoiceTran->due > 0) {
        array_push($journals, array(
          "name" => "Account Payable",
          "type" => "Dr",
          "tran_type" => "invoice",
          "description" => "Product return (supplier)",
          "tran_id" => $invoiceTran->id,
          "amount" => $invoiceTran->due,
        ));
      }
    }

    return $this->saveJournal($journals);
  }

  public function generateJournalFromPartnerOrInvestor($dbTable = "investors_balance_histories", $amount = 0, $dbId = 0, $action = "add")
  {

    $journals = array();

    if ($action == "add") {
      array_push($journals, array(
        "name" => "Cash",
        "type" => "Dr",
        "description" => $dbTable == "investors_balance_histories" ? "Investment Invest" : "Capital In",
        "tran_type" => $dbTable,
        "tran_id" => $dbId,
        "amount" => $amount
      ));

      array_push($journals, array(
        "name" => $dbTable == "investors_balance_histories" ? "Investment" : "Capital",
        "type" => "Cr",
        "tran_type" => $dbTable,
        "tran_id" => $dbId,
        "amount" => $amount
      ));
    } else {

      array_push($journals, array(
        "name" => $dbTable == "investors_balance_histories" ? "Investment" : "Withdraw",
        "type" => "Dr",
        "tran_type" => $dbTable,
        "tran_id" => $dbId,
        "amount" => $amount
      ));

      array_push($journals, array(
        "name" => "Cash",
        "type" => "Cr",
        "description" => $dbTable == "investors_balance_histories" ? "Investment Withdraw" : "Capital Withdraw",
        "tran_type" => $dbTable,
        "tran_id" => $dbId,
        "amount" => $amount
      ));
    }

    return $this->saveJournal($journals);
  }

  public function getJournals()
  {
    $mainJournalList = array();
    $main_cr = 0;
    $main_dr = 0;
    $journal_list = Journal::where('status', 1)
      ->select('name')
      ->distinct()
      ->get();


    for ($i = 0; $i < count($journal_list); $i++) {

      $select_journal  = Journal::where([['status', 1], ["name", $journal_list[$i]->name]])->get();

      $subJournal = array();
      $cr_ballence = 0;
      $dr_ballence = 0;

      // dump(" -------- ". $journal_list[$i]->name ." ----------------");
      for ($j = 0; $j < count($select_journal); $j++) {

        $has_similer_siblings_journal = false;
        $siblings_journal  = Journal::where([
          ['status', 1],
          ["type", "!=", $select_journal[$j]->type],
          ["tran_type", "=", $select_journal[$j]->tran_type],
          ["tran_id", "=", $select_journal[$j]->tran_id],
        ])->get();

        $check_similer_siblings_journal  = Journal::where([
          ['status', 1],
          ["type", "=", $select_journal[$j]->type],
          ["tran_type", "=", $select_journal[$j]->tran_type],
          ["tran_id", "=", $select_journal[$j]->tran_id],
        ])->get();



        for ($k = 0; $k < count($siblings_journal); $k++) {

          if (count($check_similer_siblings_journal) > 1) {
            $siblings_journal[$k]->amount = $select_journal[$j]->amount;
          }

          if ($select_journal[$j]->type == "Dr") {
            $dr_ballence += $siblings_journal[$k]->amount;
          } else {
            $cr_ballence += $siblings_journal[$k]->amount;
          }
          // dump($siblings_journal[$k]->name ."  =  ". $siblings_journal[$k]->amount  . " = " . $siblings_journal[$k]->type);
          // dump(" ** Balance Cr -- ". $dr_ballence ." =  Dr " . $cr_ballence);
          array_push($subJournal, $siblings_journal[$k]);
        }
      }

      $main_cr += $cr_ballence;
      $main_dr += $dr_ballence;

      if ($dr_ballence > $cr_ballence) {
        $final_tran_type  =  "Dr";
        $final_amount  =  $dr_ballence - $cr_ballence;
      } else {
        $final_tran_type  =  "Cr";
        $final_amount  =  $cr_ballence - $dr_ballence;
      }


      array_push($mainJournalList, array(
        "name" => $journal_list[$i]->name,
        "data" =>  $subJournal,
        "final_tran_type" => $final_tran_type,
        "final_amount" => $final_amount
      ));

      // dump(" ### Final -- ". $final_tran_type ." = " . $final_amount);
    }

    // dump("main_cr : " . $main_cr);
    // dump("main_dr : " . $main_dr);

    // dump($mainJournalList);
    // dd("------");

    return array("journal" => $mainJournalList, "main_cr" => $main_cr, "main_dr" => $main_dr);
  }

  public function getTrialBalanceEntity()
  {
    $entity = array(
      array(
        "name" => "Account Payable",
        "type" => "Cr",
        "amount" => 0
      ),
      array(
        "name" => "Account Receivable",
        "type" => "Dr",
        "amount" => 0
      ),
      array(
        "name" => "Capital",
        "type" => "Cr",
        "amount" => 0
      ),
      array(
        "name" => "Cash",
        "type" => "Dr",
        "amount" => 0
      ),
      array(
        "name" => "Damage",
        "type" => "Dr",
        "amount" => 0
      ),
      array(
        "name" => "Investment",
        "type" => "Cr",
        "amount" => 0
      ),
      array(
        "name" => "Purchase Return",
        "type" => "Cr",
        "amount" => 0
      ),
      array(
        "name" => "Purchase",
        "type" => "Dr",
        "amount" => 0
      ),
      array(
        "name" => "Expense",
        "type" => "Dr",
        "amount" => 0
      ),
      array(
        "name" => "Sales",
        "type" => "Cr",
        "amount" => 0
      ),
      array(
        "name" => "Sales Return",
        "type" => "Dr",
        "amount" => 0
      ),
      array(
        "name" => "Withdraw",
        "type" => "Dr",
        "amount" => 0
      ),
    );

    return $entity;
  }

  public function getIncomeStatementEntity()
  {
    $entity = array(
      "Sales Income" => array(

        array(
          "name" => "Sales",
          "cal" => '+',
          "amount" => 0
        ),

        array(
          "name" => "Sales Return",
          "cal" => '-',
          "amount" => 0
        )

      ),

      "Cost Of Goods Sold" => array(

        array(
          "name" => "Purchase",
          "cal" => '+',
          "amount" => 0
        ),

        array(
          "name" => "Purchase Return",
          "cal" => '-',
          "amount" => 0
        ),

      ),

      "Expense" => array(

        array(
          "name" => "Damage",
          "cal" => '+',
          "amount" => 0
        ),

        array(
          "name" => "Expense",
          "cal" => '+',
          "amount" => 0
        ),

      )
    );

    return $entity;
  }

  public function getIncomeStatement()
  {
    $jaurnalData = $this->getJournals();
    $incomeStatementEntityData = $this->getIncomeStatementEntity();

    $mainJournalList =  $jaurnalData['journal'];
    $total_sales_income = 0;
    $total_cost_of_goods_sold = 0;
    $total_expense = 0;


    for ($j = 0; $j < count($incomeStatementEntityData["Sales Income"]); $j++) {
      for ($k = 0; $k < count($mainJournalList); $k++) {
        if ($incomeStatementEntityData["Sales Income"][$j]['name'] == $mainJournalList[$k]['name']) {
          $incomeStatementEntityData["Sales Income"][$j]['amount'] = $mainJournalList[$k]['final_amount'];

          // Sum Total Sales
          if ($incomeStatementEntityData["Sales Income"][$j]['cal'] == '+') {
            $total_sales_income += $incomeStatementEntityData["Sales Income"][$j]['amount'];
          } else {
            $total_sales_income -= $incomeStatementEntityData["Sales Income"][$j]['amount'];
          }

          break;
        }
      }
    }

    for ($j = 0; $j < count($incomeStatementEntityData["Cost Of Goods Sold"]); $j++) {
      for ($k = 0; $k < count($mainJournalList); $k++) {
        if ($incomeStatementEntityData["Cost Of Goods Sold"][$j]['name'] == $mainJournalList[$k]['name']) {
          $incomeStatementEntityData["Cost Of Goods Sold"][$j]['amount'] = $mainJournalList[$k]['final_amount'];

          // Sum Total total_cost_of_goods_sold
          if ($incomeStatementEntityData["Cost Of Goods Sold"][$j]['cal'] == '+') {
            $total_cost_of_goods_sold += $incomeStatementEntityData["Cost Of Goods Sold"][$j]['amount'];
          } else {
            $total_cost_of_goods_sold -= $incomeStatementEntityData["Cost Of Goods Sold"][$j]['amount'];
          }

          break;
        }
      }
    }

    for ($j = 0; $j < count($incomeStatementEntityData["Expense"]); $j++) {
      for ($k = 0; $k < count($mainJournalList); $k++) {
        if ($incomeStatementEntityData["Expense"][$j]['name'] == $mainJournalList[$k]['name']) {
          $incomeStatementEntityData["Expense"][$j]['amount'] = $mainJournalList[$k]['final_amount'];

          // Sum Total Expense
          if ($incomeStatementEntityData["Expense"][$j]['cal'] == '+') {
            $total_expense += $incomeStatementEntityData["Expense"][$j]['amount'];
          } else {
            $total_expense -= $incomeStatementEntityData["Expense"][$j]['amount'];
          }

          break;
        }
      }
    }

    return array(
      "IncomeStatementEntityData" => $incomeStatementEntityData,
      "Total Sales Income" => $total_sales_income,
      "Total Cost Of Goods Sold" => $total_cost_of_goods_sold,
      "Total Expense" => $total_expense
    );
  }

  public function getOwnerEquity()
  {
    $journalData = $this->getJournals();
    $incomeStatement = $this->getIncomeStatement();


    $total_sales_income = $incomeStatement['Total Sales Income'];
    $total_COGS = $incomeStatement['Total Cost Of Goods Sold'];
    $total_expense = $incomeStatement['Total Expense'];

    $netProfite = $total_sales_income - $total_COGS - $total_expense;
    $capital = 0;
    $withdraw = 0;

    for ($i = 0; $i < count($journalData['journal']); $i++) {
      if ($journalData['journal'][$i]['name'] == 'Capital') {
        $capital = $journalData['journal'][$i]['final_amount'];
      } else if ($journalData['journal'][$i]['name'] == 'Withdraw') {
        $withdraw = $journalData['journal'][$i]['final_amount'];
      }
    }

    return array(
      "netProfite" => $netProfite,
      "capital" => $capital,
      "withdraw" => $withdraw,
    );
  }


  public function getBalanceSheetEntity()
  {
    $entity = array(
      "Assets" => array(

        array(
          "name" => "Cash",
          "amount" => 0
        ),

        array(
          "name" => "Bank",
          "amount" => 0
        ),

        array(
          "name" => "Account Receivable",
          "amount" => 0
        )

      ),

      "Fixed Assets" => array(

        array(
          "name" => "Furniture",
          "amount" => 0
        ),

        array(
          "name" => "Equipment",
          "amount" => 0
        ),

      ),

      "Liability" => array(

        array(
          "name" => "Account Payable",
          "amount" => 0
        ),

        array(
          "name" => "Capital",
          "amount" => 0
        ),

        array(
          "name" => "Investment",
          "amount" => 0
        ),

      )
    );

    return $entity;
  }

  public function getBalanceSheet()
  {
    $jaurnalData = $this->getJournals();
    $balanceSheetEntityData = $this->getBalanceSheetEntity();

    $mainJournalList =  $jaurnalData['journal'];
    $total_assets = 0;
    $total_fixed_assets = 0;
    $total_liability = 0;


    for ($j = 0; $j < count($balanceSheetEntityData["Assets"]); $j++) {
      for ($k = 0; $k < count($mainJournalList); $k++) {
        if ($balanceSheetEntityData["Assets"][$j]['name'] == $mainJournalList[$k]['name']) {
          $balanceSheetEntityData["Assets"][$j]['amount'] = $mainJournalList[$k]['final_amount'];

          // Sum Assets
          $total_assets += $balanceSheetEntityData["Assets"][$j]['amount'];
          break;
        }
      }
    }

    for ($j = 0; $j < count($balanceSheetEntityData["Fixed Assets"]); $j++) {
      for ($k = 0; $k < count($mainJournalList); $k++) {
        if ($balanceSheetEntityData["Fixed Assets"][$j]['name'] == $mainJournalList[$k]['name']) {
          $balanceSheetEntityData["Fixed Assets"][$j]['amount'] = $mainJournalList[$k]['final_amount'];

          // Sum Total total_fixed_assets
          $total_fixed_assets += $balanceSheetEntityData["Fixed Assets"][$j]['amount'];

          break;
        }
      }
    }

    $ownerEquity  = $this->getOwnerEquity();
    $netProfite = $ownerEquity['netProfite'] + $ownerEquity['capital'] - $ownerEquity['withdraw'];

    for ($j = 0; $j < count($balanceSheetEntityData["Liability"]); $j++) {
      for ($k = 0; $k < count($mainJournalList); $k++) {
        if ($balanceSheetEntityData["Liability"][$j]['name'] == $mainJournalList[$k]['name']) {

          if ($balanceSheetEntityData["Liability"][$j]['name'] == 'Capital') {
            $balanceSheetEntityData["Liability"][$j]['amount'] =  $netProfite;
          } else {
            $balanceSheetEntityData["Liability"][$j]['amount'] = $mainJournalList[$k]['final_amount'];
          }

          // Sum Total Liability
          $total_liability += $balanceSheetEntityData["Liability"][$j]['amount'];
          break;
        }
      }
    }



    return array(
      "IncomeStatementEntityData" => $balanceSheetEntityData,
      "Total Assets" => $total_assets,
      "Total Fixed Assets" => $total_fixed_assets,
      "Total Liability" => $total_liability
    );
  }

  // public function userUniqueCheck(Request $request)
  // {
  //     $user = $request->query('username');
  //     if ($user) {
  //         $get_username = User::where('username', $user)->count();
  //         if ($get_username > 0) {
  //             return response("false");
  //         }
  //         else {
  //             return response("true");
  //         }
  //     }
  //     else {
  //         return response("false");
  //     }
  // }

  public function getMonthsName()
  {
    $monthsName = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    return $monthsName;
  }

  public function generateInvoicePDF($invoice_tran_id)
  {

    $invoice_details = InvoiceTranItem::where([['status', '=', 1], ['invoice_tran_id', '=', $invoice_tran_id]])->get();

    //************Add product Item in invoice***************** */

    $subtotal2 = 0;
    for ($i = 0; $i < count($invoice_details); $i++) {


      $temp_qty = $invoice_details[$i]->qty;

      if (strtoupper($invoice_details[$i]->quantity_as) == 'TON') {
        $temp_qty = $invoice_details[$i]->qty * 907.185;
      } else if (strtoupper($invoice_details[$i]->quantity_as) == 'BOX') {
        $temp_qty = $invoice_details[$i]->qty * $invoice_details[$i]->product->quatity_value;
      } else if (strtoupper($invoice_details[$i]->quantity_as) == 'KG') {
        $temp_qty = $invoice_details[$i]->qty;
      } else if (strtoupper($invoice_details[$i]->quantity_as) == 'PCS') {
        $temp_qty = $invoice_details[$i]->qty;
      } else {
        dd("404");
      }

      $invoice_details[$i]['total'] = $invoice_details[$i]->price_per_rate * $temp_qty;
      $subtotal2 += $invoice_details[$i]->price_per_rate * $temp_qty;

      // Get product Head
      if ($invoice_details[$i]->product->product_head_id != 0) {
        $product_head_details = ProductHead::where([['id', '=', $invoice_details[$i]->product->product_head_id], ['status', '=', 1]])->select('title', 'status')->first();
        if (!empty($product_head_details)) {
          $invoice_details[$i]['product_head'] = $product_head_details;
        }
      }

      // // Get product Category
      if ($invoice_details[$i]->product->product_category_id != 0) {
        $product_cat_details = ProductCategory::where([['id', '=', $invoice_details[$i]->product->product_category_id], ['status', '=', 1]])->select('title', 'status')->first();
        if (!empty($product_cat_details)) {
          $invoice_details[$i]['product_category'] = $product_cat_details;
        }
      }

      // // Get product type
      if ($invoice_details[$i]->product->product_type_id  != 0) {
        $product_type_details = ProductType::where([['id', '=', $invoice_details[$i]->product->product_type_id], ['status', '=', 1]])->select('title', 'status')->first();
        if (!empty($product_type_details)) {
          $invoice_details[$i]['product_type'] = $product_type_details;
        }
      }


      // // Get product Size
      if ($invoice_details[$i]->product->product_size_id != 0) {
        $product_size_details = ProductSize::where([['id', '=', $invoice_details[$i]->product->product_size_id], ['status', '=', 1]])->select('title', 'status')->first();
        if (!empty($product_size_details)) {
          $invoice_details[$i]['product_size'] = $product_size_details;
        }
      }


      // // Get product Group
      if ($invoice_details[$i]->product->product_group_id != 0) {
        $product_group_details = ProductGroup::where([['id', '=', $invoice_details[$i]->product->product_group_id], ['status', '=', 1]])->select('group_name', 'status')->first();
        if (!empty($product_group_details)) {
          $invoice_details[$i]['product_group'] = $product_group_details;
        }
      }
    }


    //***************************** */

    $invoiceTranDetails = InvoiceTran::where([['status', '=', 1], ['id', '=', $invoice_tran_id]])->get();

    // Supplier or Customer Details
    if ($invoiceTranDetails[0]->customer_id != null &&  $invoiceTranDetails[0]->supplier_id == null) {
      $customer_info = Customer::where([['status', '=', 1], ['id', '=', $invoiceTranDetails[0]->customer_id]])->get();
      $invoice_details->name = $customer_info[0]->name;
      $invoice_details->phone_no = $customer_info[0]->contact_no;
    }
    if ($invoiceTranDetails[0]->customer_id == null &&  $invoiceTranDetails[0]->supplier_id != null) {
      $supplier_info = Supplier::where([['status', '=', 1], ['id', '=', $invoiceTranDetails[0]->supplier_id]])->get();
      $invoice_details->name = $supplier_info[0]->name;
      $invoice_details->phone_no = $supplier_info[0]->contact_no;
    }

    // extra data from invoice 
    $invoice_details->invoice_id = $invoiceTranDetails[0]->inv_no;
    $invoice_details->sub_total = $invoiceTranDetails[0]->sub_total;
    $invoice_details->total = $invoiceTranDetails[0]->total;
    $invoice_details->discount = $invoiceTranDetails[0]->discount;
    $invoice_details->paid = $invoiceTranDetails[0]->paid;
    $invoice_details->due = $invoiceTranDetails[0]->due;
    $invoice_details->date = $invoice_details[0]->created_at;


    //return $invoice_details;

    view()->share('invoice_details', $invoice_details);


    $pdf = PDF::loadView('pdf_generate.salesAndReturnsPdf', $invoice_details);

    // download PDF file with download method
    $download_invoice =  $pdf->download('pdf_file.pdf');
    $stream_invoice =  $pdf->stream('pdf_file.pdf');

    //dd($stream_invoice);
    return $stream_invoice;
  }


  function generateJournalForGeneralExpense()
  {
    //  This code is use for update or create journal for General Expense  
    // ---- Start 

    $toMonthFirstDay = date('Y-m-d');
    $search_month = 0;
    $search_year = 2021;

    $todaysMonthYear = date("F, Y");

    try {
      $tempTodaysMonthYear = join("", explode(" ", $todaysMonthYear)); // Remove space  ->  "December, 2020" to "December,2020"
      $tempTodaysMonthYear = explode(",", $tempTodaysMonthYear);

      $month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      $temp_month = array_search($tempTodaysMonthYear[0], $month, true) + 1; // 12

      // get Search Month and year
      $search_month = $temp_month - 1;
      $search_year = $tempTodaysMonthYear[1];

      // Generate  toMonthFirstDay
      $temp_month = $temp_month < 10 ? ('0' . $temp_month) : $temp_month;
      $toMonthFirstDay = $tempTodaysMonthYear[1] . "-" . $temp_month . "-" . "01"; // 2020-12-01

    } catch (\Throwable $th) {
      //throw $th;
    }

    $generalExpenseMonthsId = GeneralExpenseMonth::where('month', '=', $search_month)->select('general_expense_id');
    $generalExpense = GeneralExpense::whereIn('id', $generalExpenseMonthsId)
      ->where('status', 1)
      ->get();

    for ($i = 0; $i < count($generalExpense); $i++) {

      $journals = array();
      $willSaveNewJournal = false;

      $dr_list = Journal::where([['status', 1], ["tran_type", '=', 'expense'], ["type", "=", "Dr"], ["tran_id", '=', $generalExpense[$i]->id]])
        ->get();

      if (count($dr_list) < 1) {
        array_push($journals, array(
          "name" => "Expense",
          "type" => "Dr",
          "description" =>  "General Expense for " .  $generalExpense[$i]->title,
          "tran_type" => "expense",
          "tran_id" => $generalExpense[$i]->id,
          "amount" => $generalExpense[$i]->amount,
          "created_at" => $toMonthFirstDay,
          "updated_at" => $toMonthFirstDay
        ));

        $willSaveNewJournal = true;
      } else {
        $dr_list[0]->description =  "General Expense for " . $generalExpense[$i]->title;
        $dr_list[0]->amount = $generalExpense[$i]->amount;
        $dr_list[0]->created_at = $toMonthFirstDay;
        $dr_list[0]->updated_at = $toMonthFirstDay;
        $dr_list[0]->save();
      }


      $cr_list = Journal::where([['status', 1], ["tran_type", '=', 'expense'], ["type", "=", "Cr"], ["tran_id", '=', $generalExpense[$i]->id]])
        ->get();

      if (count($cr_list) < 1) {
        array_push($journals, array(
          "name" => "Cash",
          "type" => "Cr",
          "description" =>  "General Expense for " .  $generalExpense[$i]->title,
          "tran_type" => "expense",
          "tran_id" => $generalExpense[$i]->id,
          "amount" => $generalExpense[$i]->amount,
          "created_at" => $toMonthFirstDay,
          "updated_at" => $toMonthFirstDay
        ));
        $willSaveNewJournal = true;
      } else {
        $cr_list[0]->description =  "General Expense for " . $generalExpense[$i]->title;
        $cr_list[0]->amount = $generalExpense[$i]->amount;
        $cr_list[0]->created_at = $toMonthFirstDay;
        $cr_list[0]->updated_at = $toMonthFirstDay;
        $cr_list[0]->save();
      }


      if ($willSaveNewJournal == true) {
        // dd($journals);
        $this->saveJournal($journals);
      }
    }

    // ---- End 
  }
}
