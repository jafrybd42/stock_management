<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExpenseController extends Controller
{

    private $common_class_obj = "";

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::where('status', 1)->orderBy('id', 'DESC')->get();
        return view('expense.index', compact('expenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedata = $request->validate([
            'title' => 'required|max:255|min:3',
            'amount' => 'required|min:1|numeric',
            'date' => 'required',
        ]);

     
        $expense = new Expense();
        $expense->title = $request->title;
        $expense->amount = $request->amount;
        $expense->date = $request->date;
        $expense->created_by = Auth::user()->id;
        $expense->updated_by = Auth::user()->id;
        $expense->save();


        if ($expense) {
            return redirect()->route('expense.index')->with('success', 'Product Size Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense_details = Expense::findorFail($id);
        return view('expense.edit', compact('expense_details', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedata = $request->validate([
            'title' => 'required|max:255|min:3',
            'amount' => 'required|min:1|numeric',
            'date' => 'required',
        ]);

        $expense = Expense::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('expense.index')->with('error', 'Expense Unknown');
        }

        $expense->title = $request->title;
        $expense->amount = $request->amount;
        $expense->date = $request->date;
        $expense->updated_by = Auth::user()->id;
        $expense->save();

        if ($expense) {
            return redirect()->route('expense.index')->with('success', 'Expense Details Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_size = Expense::findorFail($id);

        $product_size->status = 0;
        $product_size->save();

        return redirect()->route('expense.index')->with('success', 'Expense Deleted');
    }
}
